
## Devops documentation
The following repository is to provide information about application deployment, installation and tuning.

- MarkDown format  : **[./documentation](./documentation)**
- Plain text format: **[./plain_Text](./plain_text)**
- Infrasctructure helpful scripts: **[./scripts](./scripts)**
