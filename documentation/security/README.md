
# Security

This file will contain information related to tuning and security tips to be applied on a server.

Glossary:

- [Open SSH](#open-ssh)

## Open SSH

- To disable password authentication and permit root login:
	- Modify the file **`/etc/ssh/sshd_config`** and set the following parameters:
	```
	PermitRootLogin no
	PasswordAuthentication no
	```
