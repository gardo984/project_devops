
# Helm K8s Package Manager

Glosary:
- [Installation](#installation)
- [Useful Commands](#useful-commands)
- [Create Chart](#create-chart)
- [Chart Templates](#chart-templates)
- [Chart Hooks](#chart-hooks)
- [Additional Packages](#additional-packages)
- [References](#references)

## Installation

- Perform the following commands:
```sh
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
- If you have issues connecting to the cluster, perform the following command and try one more time:
```sh
kubectl config view --raw >~/.kube/config
```

## Useful Commands

> ### Important!
> A good common repository is `bitnami` : https://charts.bitnami.com/bitnami , it can be added with the following command:
> ```sh
> helm repo add bitnami https://charts.bitnami.com/bitnami
> helm repo list
> ```

- Check current version in a verbose mode:
```sh
helm version --debug
```
- List releases:
```sh
helm list
```
- Search a package (as a example `wordpress` package):
```sh
helm search hub wordpress
```
- Add a repository and install a package (as a example `wordpress` package):
```sh
helm repo add bitnami-azure https://marketplace.azurecr.io/helm/v1/repo
helm install my-release bitnami-azure/wordpress
```
- Remove a package:
```sh
helm uninstall my-release
```
- Pull package from repository:
```sh
helm pull bitnami-azure/wordpress
```
- Decompress downloaded package:
```sh
helm pull --untar bitnami-azure/wordpress
```
- Install command:
	- Install package overwriting some of its parameters:
	```sh
	helm install --set <parameter-name>=<value> --set <parameter-name>=<value> bitnami-azure/wordpress
	```
	- Install package overwriting some of its parameters based on **`custom-values.yaml`** file:
	```sh
	helm install --values custom-values.yaml bitnami-azure/wordpress
	```
	- Install the package referencing the chart downloaded directory:
	```sh
	helm install my-release ./wordpress
	```
	- Simulate a chart installation:
	```sh
	helm install my-release ./wordpress --dry-run
	```
- Upgrade a release:
```sh
helm upgrade my-release <chart-repo>
```
- List all revisions per release:
```sh
helm history my-release
```
- Perform a rollback based on a specific revision (history):
```sh
helm rollback my-release <revision-index>
```
- Pull dependencies of a chart (for this example `alfresco-content-services`):
```sh
helm dependency build alfresco-content-services
```

## Create Chart

- To create a skeleton for a helm chart:
```sh
helm create nginx-chart
ls nginx-chart/
```
- Validate all templates before chart installation:
```sh
helm template <release-name> <chart-name> --debug
```
- Validate a specific template before chart installation:
```sh
helm template <release-name> <chart-name> -s templates/deployment.yaml --debug
```

## Chart Templates

Templates directory store all k8s resources that will be created such as: `Deployments`, `Services`, `Ingress`, etc. 

- Inspect templates directory:
```sh
ls <chart-directory>/templates/
```
- There is a file called `_helpers.tpl` which store functions / methods can be invoked from the templates:
```sh
ls <chart-directory>/templates/_helpers.tpl
```
- There is a file called `values.yaml` which stores values that can be customized in the `Chart`:
```sh
ls <chart-directory>/values.yaml
```
- There is a file called `Chart.yaml` which stores values related to the chart such as: app version, chart version. 
```sh
ls <chart-directory>/Chart.yaml
```
- This is how a template looks like:
```yaml
{{- if .Values.configmap.create }}
apiVersion: v1
data:
   fruits:
   {{- range .Values.configmap.labels }}
   - {{ . |title| quote }}
   {{- end }}
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: {{ .Release.Name }}
{{- end }}
```
- Use of `template` and `include`:
	> ### Important
	> The function `template` allows to inject content in a template but it can not be used with pipes such as : `nindent`, `upper`, etc.
	
	- Define a helper in `_helpers.tpl`:
	```
	{{- define "deployment.labels" -}}
	app-name: hello-world
	tier: backend
	deployment-server: host1.domain.com
	{{- end }}
	```
	- Create a deployment file, in a scenario you have created a `Chart` called `hello-world`:
	```sh
	kubectl create deployment hello-world \
		--replicas=2 \
		--image=helloworld.iso \
		--dry-run=client -o yaml > hello-world/templates/test-helloworld-deployment.yaml
	```
	- Modify the file to have the following structure:
	```yaml
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  creationTimestamp: null
	  labels:
	    app: hello-world
	  name: hello-world
	spec:
	  replicas: 2
	  selector:
	    matchLabels:
	      app: hello-world
	      {{- include "deployment.labels" . | nindent 6 }}
	  strategy: {}
	  template:
	    metadata:
	      creationTimestamp: null
	      labels:
	        app: hello-world
	        {{- include "deployment.labels" . | nindent 8 }}
	    spec:
	      containers:
	      - image: helloworld.iso
	        name: helloworld-iso-6j8d9
	        resources: {}
	status: {}
	```
	- Validate that changes in the yaml file are correct:
	```sh
	$ helm template hello-release hello-world -s templates/test-helloworld-deployment.yaml
	---
	# Source: hello-world/templates/test-helloworld-deployment.yaml
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  creationTimestamp: null
	  labels:
	    app: hello-world
	  name: hello-world
	spec:
	  replicas: 2
	  selector:
	    matchLabels:
	      app: hello-world
	      app-name: hello-world
	      tier: backend
	      deployment-server: host1.domain.com
	  strategy: {}
	  template:
	    metadata:
	      creationTimestamp: null
	      labels:
	        app: hello-world
	        app-name: hello-world
	        tier: backend
	        deployment-server: host1.domain.com
	    spec:
	      containers:
	      - image: helloworld.iso
	        name: helloworld-iso-6j8d9
	        resources: {}
	status: {}

	```


## Chart Hooks

Every time one the following commands is executed:
```sh
helm install <chart-name>
helm upgrade <chart-name>
helm rollback <chart-name>
helm delete <chart-name>
```
An event can be triggered `pre` or `post` the command, as a way to reach a demostration, please follow the next example:
- Create a job resource:
```sh
kctl create job hello-world-job \
	--image=hello-world.iso \
	--dry-run=client -o yaml  -- echo -n "hello world" > hello-world/templates/backup-job.yaml
```
- Modify the template:
> ### Important
> Pay attention to the `annotations` field, values can vary from `pre-<command>`, `post-<command>`:
>  - pre-upgrade
>  - post-upgrade
>  - pre-install
>  - post-install
>  - pre-delete
>  - post-delete
>  - pre-rollback
>  - post-rollback
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: {{ .Release.Name }}
  annotations:
   "helm.sh/hook": pre-upgrade,pre-install
   "helm.sh/hook-weight": "5"
   "helm.sh/hook-delete-policy": hook-succeeded
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - command:
        - echo
        - -n
        - hello world
        image: "alpine"
        name: {{ .Release.Name }}-job
        resources: {}
      restartPolicy: Never
status: {}
```
- Perform the operation and pay attention of the action.

## Publish Chart

- To generate a `gpg` in dev environment:
```sh
gpg --quick-generate-key "John Smith"
```
- To generate a `gpg` in production environment:
```sh
gpg --full-generate-key "John Smith"
```
- List gpg keys:
```sh
gpg --list-keys
```
- Export new gpg format to old format:
```sh
gpg --export-secret-keys "key name"> oldformat.gpg
```
- Generate the public key based on the created gpg:
```sh
gpg --export "key name" > mypublickey
```
- Package a `Chart` project:
	- Without gpg:
	```sh
	helm package <chart-directory>
	```
	- With gpg (a file with an extension `.prov` should be created):
	```sh
	helm package --sign --key 'key name' --keyring gpgfile <chart-directory>
	```
- Get hash from a file:
```sh
sha265sum <chart-tgz>
```
- Verify chart integrity:
```sh
helm verify <chart-directory>
helm verify --keyring ./mypublickey <chart-directory>
```
- Verify signature during installation:
```sh
helm install --verify <chart-name>
```
- To generate the chart index file of the package:
```sh
mkdir <chart-name>-files
cp <chart>.tgz <chart>.tgz.provn <chart-name>-files/
helm repo index <chart-name>-files/ --url https://<repository-host>
```

## Additional Packages

### Nginx Ingress

- Installation:
```sh
helm repo add nginx https://helm.nginx.com/stable
helm install my-nginx-ingress nginx/nginx-ingress --version 0.18.1
```
- To validate:
```sh
kubectl get ingressclass
```

### NFS subdir external provisioner

- Installation:
```sh
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
helm repo update
helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner --set nfs.server=<nfs-server> --set nfs.path=<nfs-shared-path>
```

### Cert Manager

- Installation:
```sh
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.7.1 --set installCRDs=true
```
- Create a `ClusterIssuer` resource to process `CertificateRequests`:
>#### Important
> Be careful with the value of `<ingress-class>`, the value can be identified performing the command:
> ```sh
> kubectl get ingressclass -A
> ```
> If you have more than one `IngressClass` you can choose the correct one, otherwise just the default one that is displayed with the previous command.

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: <cluster-issuer-name>
spec:
  acme:
    #server: https://acme-staging-v02.api.letsencrypt.org/directory
    server: https://acme-v02.api.letsencrypt.org/directory
    email: <email>
    privateKeySecretRef:
      name: <private-key-name>
    solvers:
      - http01:
          ingress:
            class: <ingress-class>
```
- Create an `Ingress` resource to publish the `Kubernetes Dashboard` and specify a `ClusterIssuer` in order to get a SSL certificate for the `<host>` specified:
>#### Important
> Take into consideration that `ClusterIssuer` works globally in all the namespaces contrary to `Issuer` that works only for resources in the same namespace.
```yaml
apiVersion: networking.k8s.io/v1 
kind: Ingress
metadata:
  annotations:
    cert-manager.io/cluster-issuer: <cluster-issuer-resource>
    kubernetes.io/ingress.class: public 
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
  name: <ingress-name>
  namespace: kube-system
spec:
  rules:
  - host: <host>
    http:
      paths:
      - backend:
          service:
           name: kubernetes-dashboard
           port:
            number: 443
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - <host>
    secretName: <ingress-name>-cert
```

## References

- String Functions: https://helm.sh/docs/chart_template_guide/function_list/#string-functions
- Control Structures: https://helm.sh/docs/chart_template_guide/control_structures/
- Functions and Pipelines: https://helm.sh/docs/chart_template_guide/functions_and_pipelines/
- Artifact Chart repository: https://artifacthub.io/packages/search?kind=0
- Chart Hooks: https://helm.sh/docs/topics/charts_hooks/