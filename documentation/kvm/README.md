# KVM
The information covered in the current document will be related to:
- [Previous Requirements and Configurations](#previous-requirements-and-configurations).
	- [Virtualization checks](#virtualization-checks).
	- [Network configurations](#network-configurations).
	- [Ssh configurations](#ssh-configurations).
- [Installation](#installation).
	- [Server Side](#server-side).
	- [Client Side](#client-side).
- [Usage](#usage).
	- [Server Side Operations](#server-side-operations).
	- [Client Side Operations](#client-side-operations).
## Previous Requirements and Configurations.

### Virtualization checks
- Check if you are on a **virtual** or **physic** host with one of the following options:
```sh
sudo dmidecode system |less
dmes|grep -i virtual
lscpu |grep -i hypervisor
hostnamectl status |grep -i virtual
```
- Check if the server **supports virtualization**:
```sh
egrep -c "(svm|vmx)" /proc/cpuinfo
```
- Check if the server **supports 64 bits arquitecture**:
```sh
egrep -c " lm " /proc/cpuinfo
uname -r
```
### Network Configurations
- Perform the following **Network Configurations**:
	- Some helpful commands to find out the **MAC address**, in the second example you should replace **`<net_interface>`**, by your local interface, such as : **`enp0s3`**,**`eno<serial_numbers>`**.
	```sh
	ip link show
	```
	```sh
	cat /sys/class/net/<net_interface>/address
	```
#### CentOS / Rocky Linux

- Setup a bridge configuration, you should replace the values of the fields **`DNS1`**, **`GATEWAY`**, **`IPADDR`**, **`NETMASK`**:
```sh
cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-br0
DEVICE="br0"
TYPE="Bridge"
BOOTPROTO="static"
DNS1="<dns_host>"
GATEWAY="<gateway_address>"
IPADDR="<host_address>"
NETMASK="<netmask>"
ONBOOT="yes"
EOF
```
- Setup a ethernet configuration, you should replace the values of the fields **`HWADDR`**:
```sh
cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE="enp0s3"
NAME="enp0s3"
HWADDR="<here_your_mac_address>"
ONBOOT="yes"
TYPE="Ethernet"
IPV6INIT="no"
BRIDGE="br0"
EOF
```

#### Ubuntu

If you are working in `Ubuntu 22`, you will have to configure the interface with [netplan](https://netplan.readthedocs.io/en/stable/) the new network configuration file located in the file `/etc/netplan/`:

- Create the file if not exists `/etc/netplan/01-netcfg.yaml` with the following content:
```yaml
network:
  version: 2
  renderer: networkd

  ethernets:
    eno1:
      dhcp4: false
      dhcp6: false 

  bridges:
    br0:
      addresses: [192.168.0.8/24]
      interfaces: [eno1]
      routes:
      - to: default
        via: 192.168.0.1
        metric: 100
        on-link: true
      nameservers:
        addresses: [8.8.8.8]
      parameters:
        forward-delay: 4
        stp: false
```
```sh
sudo netplan generate
sudo netplan --debug appy
```
- Proceed to restart the network:
```sh
sudo nmcli network off
sudo nmcli network on
```
- Validate that the interface was created:
```sh
ifconfig br0
# or
brctl show
```
- Proceed to create the network interface on kvm bound to the bridge interface created:
	- Create a file named `br0.xml` with the following content:
	```xml
	<network>
	  <name>host-bridge</name>
	  <forward mode="bridge"/>
	  <bridge name="br0"/>
	</network>
	```
	```sh
	virsh net-define br0.xml
	virsh net-start host-bridge
	virsh net-autostart host-bridge
	```
	- Validate that interface was created and it is activated:
	```sh
	virsh net-list
	```

- **Optional**, if you are interested to create the interface with `libvirtd`, create a file with the following content and start the interface on kvm:
```xml
<network connections='1'>
  <name>br0</name>
  <uuid>cf47616c-fd46-4b8f-87d9-73043ba911df</uuid>
  <forward dev='eno1' mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
    <interface dev='eno1'/>
  </forward>
  <bridge name='virbr1' stp='on' delay='0'/>
  <mac address='52:54:00:f8:00:97'/>
  <domain name='br0'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.128' end='192.168.100.254'/>
    </dhcp>
  </ip>
</network>
```
```sh
virsh net-define br0.xml
virsh net-start br0
virsh net-autostart br0
brctl show
```

#### Validate Interface

- Restart network service:
	- **Important**: if you are having issues related to the interface name, please be sure that the device have been renamed to **`enp0s3`**, otherwise proceed with the following steps to rename the device:
	```sh
	# identify the device
	ip link show
	# turn off the device
	ip link set eth0 down
	# rename the device
	ip link set eth0 enp0s3
	# turn on the device
	ip link set eth0 up
	# restart the network service
	systemctl stop network && systemctl start network
	ip addr show
	```
```sh
systemctl stop network && systemctl start network
```
- Validate that configurations took effect:
```sh
ip addr show enp0s3 && ip addr show br0
```
- **Optional**, for debugging you can use the following commands:
```sh
ifconfig
ip -j -p -d link show br0
```

### Ssh Configurations
- The following configuration are for those who make deployments through **KVM remote servers**.
	- Check if the following setting is applied **`X11Forwarding yes`**, if not, please update it and restart the sshd service **`systemctl restart sshd`**:
	```sh
	cat /etc/ssh/sshd_config  |grep -i x11forwarding
	```
	- Generate a **`ssh RSA key`** for ssh authentication:
	```sh
	ssh-keygen -t rsa -f ~/.ssh/id_rsa_local
	```
	- Copy the **`ssh key`** to the **`remote Kvm server`** (**if the terminal request a password, please complete it**):
	```sh
	ssh-copy-id -i ~/.ssh/id_rsa_local.pub account@kvm-remote-host
	```
	- Add the **`id_rsa key`** to the **`ssh agent`**:
	```sh
	ssh-add ~/.ssh/id_rsa_local
	```
	- Once completed, give a try to connect to the remote server (**the terminal should not ask for a password this time**):
	```sh
	ssh account@kvm-remote-host
	```

## Installation
### Server Side
The following commands should executed in the kvm server:
- X11 packages:
```sh
sudo yum install -y xorg-x11-server-Xorg xorg-x11-xauth xorg-x11-apps bash-completion bind-utils net-tools
```
- Kvm packages:
```sh
sudo yum install -y qemu-kvm qemu-img virt-manager libvirt libvirt-python libvirt-client virt-install
```
- Copy the autocomplete file **`bash-completion`** to **`/etc/bash_completion.d/`**:
```sh
\cp -av /usr/share/bash-completion/completions/vsh /etc/bash_completion.d/virsh_bash_completion
```
### Client Side
The following commands should be executed on your **local / personal workstation**.
- Install the **`virt-viewer`**:
	- **Centos / RedHat / Fedora** distributions:
	```sh
	sudo yum install virt-viewer
	```
	- **Debian / Ubuntu** distributions:
	```sh
	sudo apt-get install virt-viewer
	```
	- **Mac OS**:
	```sh
	brew tap jeffreywildman/homebrew-virt-manager
	brew install virt-manager virt-viewer
	```

## Usage
### Server Side Operations 
- List the OS that KVM supports:
```sh
osinfo-query os
```
- Create a directory to store your images locally:
```sh
mkdir /var/images/ && chown root:root /var/images/ && chmod u+xrw /var/images/
```
- The directory where all the virtual disk are store is on **`/var/lib/libvirt/images/`**:
```sh
cd /var/lib/libvirt/images/
```
- Create a virtual machine:
	- Command in a single line:
	```sh
	virt-install --name=centos7 --ram=1024 --vcpu=1 --cdrom=/var/images/CentOS-7-x86_64-DVD-1503-01.iso --os-type=linux --os-varian=rhel7 --network bridge=br0 --graphics=spice --disk path=/var/lib/libvirt/images/centos7.dsk,size=4
	```
	- Command spread in details:
	```sh
	virt-install --name=centos7 --ram=1024 --vcpu=1 \
		--cdrom=/var/images/CentOS-7-x86_64-DVD-1503-01.iso \
		--os-type=linux --os-varian=rhel7 --network bridge=br0 \
		--graphics=spice \
		--disk path=/var/lib/libvirt/images/centos7.dsk,size=4
	```
	- If you want to reconnect later and do not let the console keep waiting interactively, add the following flag `--noautoconsole` at the end of the command, such as:
	```sh
	virt-install --name=centos7 --ram=1024 --vcpu=1 \
		--cdrom=/var/images/CentOS-7-x86_64-DVD-1503-01.iso \
		--os-type=linux --os-varian=rhel7 --network bridge=br0 \
		--graphics=spice \
		--disk path=/var/lib/libvirt/images/centos7.dsk,size=4
		--noautoconsole
	```
- Common operations on virtual machines:
	- List virtual machines:
	```sh
	virsh list --all
	```
	- Turn on a virtual machine:
	```sh
	virsh start <vm-name>
	```
	- Reboot a virtual machine:
	```sh
	virsh reboot <vm-name>
	```
	- Suspend and Resume a virtual machine:
	```sh
	virsh suspend <vm-name>
	virsh resume <vm-name>
	```
	- Edit xml configuration of a virtual machine:
	```sh
	# to change the default editor to vim
	sudo EDITOR=vim
	# to change the default editor to nano
	sudo EDITOR=nano
	virsh edit <vm-name>
	```
	- Shutdown a virtual machine:
	```sh
	virsh shutdown <vm-name>
	# force shutdown
	virsh destroy <vm-name>
	```
	- Remove a virtual machine:
	```sh
	virsh undefine <vm-name>
	```
	- Check snapshots by virtual machine:
	```sh
	virsh snapshot-list --domain <vm-name>
	```
	- Dump information of a specific virtual machine:
	```sh
	virsh dumpxml --domain <vm-name> > xml_file
	```
	- Or filtered by a pattern:
	```sh
	virsh dumpxml --domain <vm-name> |grep -i source
	```
	- **Restore information** of a specific virtual machine:
	```sh
	virsh define xml_file
	```
	- Rename a virtual machine:
	```sh
	virsh domrename <vm-name>  <new-vm-name>
	```
	- Get virtual machine information:
	```sh
	virsh dominfo <vm-name>
	```
	- Set **`memory ram`** of VM:
	```sh
	virsh setmem <vm-name> 2G --config
	virsh setmaxmem <vm-name> 2G --config
	virsh shutdown <vm-name>
	virsh start <vm-name>
	```
	- Set **`vcpu`** of VM:
	```sh
	virsh setvcpus --domain <vm-name> --maximum 2 --config
	virsh setvcpus --domain <vm-name> --count 2 --config
	virsh shutdown <vm-name>
	virsh start <vm-name>
	```
	- **Create a volumen** and **attach it to a VM**:
	```sh
	virsh vol-create-as <image-pool> <vm-name>_vol_1.dsk 10G
	virsh attach-disk --domain <vm-name> \
		--source /var/lib/libvirt/images/<vm-name>_vol_1.dsk \
		--persistent --target vdb
	```
	- Remove a **volumen** from a **image pool**:
	```sh
	virsh vol-delete <volumen-name> --pool <image-pool>
	virsh vol-refresh <image-pool>
	```
	- Clone a VM:
		```sh
		virt-clone --connect qemu:///system \
			--original <vm-name> \
			--name <new-vm-name> \
			--file /var/lib/libvirt/images/<new-vm-name>.dsk
		```
		- If you get an **error message** like the following:
		```sh
		ERROR missing source information for device sdx
		```
		- Dump the information of the **`original-vm`**, edit its config and remove the references to the other disks and perform **`the clone command`** one more time.
		```sh
		virsh dumpxml <vm-name> > /tmp/backup.xml
		virsh edit <vm-name>
		virt-clone --connect qemu:///system --original <vm-name> --name <new-vm-name> --file /var/lib/libvirt/images/<new-vm-name>.dsk
		```
		- After have cloned the VM, restore the dumped configuration:
		```sh
		virsh define xml_file
		```
	- List **`image pools`**:
	```sh
	virsh pool-list
	```
	- List **`volumens`** by **`image pool`**:
	```sh
	virsh vol-list <image-pool>
	```
	- more information related to the command:
	```sh
	man virsh
	```
- Other helpful commands:
	- Get node's information:
	```sh
	virsh nodeinfo 
	```

### Client Side Operations

Before to continue with the client operations, be sure to have properly configured the following steps:
- [Ssh configurations](#ssh-configurations).
- [Client Side](#client-side)

Let's supposed that we have created a virtual machine called **`centos7`** and We want to connect to the virtual machine:
- On the **`KVM server`** perform the following command:
```sh
$ virsh list --all
 Id    Name                           State
----------------------------------------------------
 5     centos7                        running
```
- Now lets connect to the server from our **`Workstation`**, where **`<kvm-remote-host>`** is the name of the KVM server where the VMachine is hosted and **`<vm-name>`** is the virtual machine created.
	- From **`Linux`** environment:
	```sh
	virt-viewer -c qemu+ssh://<kvm-remote-host>/system <vm-name>
	```
	- From **`Mac OS`** environment:
	```sh
	virt-viewer -c "qemu+ssh://<kvm-remote-host>/system?socket=/var/run/libvirt/libvirt-sock" <vm-name>
	```

- In your local environment a program should be started and then you would be able to see the content of the VMmachine.
![Virt-Viewer](documentation/kvm/images/picture-virt-viewer.png)

- Reconnect to a virtual machine:
```sh
virt-viewer --reconnect --wait -c qemu+ssh://<kvm-remote-host>/system <vm-name>
```
- Reconnect / Connect with a different **`ssh account`**:
```sh
virt-viewer --reconnect --wait -c qemu+ssh://<another_account>@<kvm-remote-host>/system <vm-name>
```

## References and Tips

- Incidents related to VMWare about **`promiscuous mode`**, please check the following url: https://kb.vmware.com/s/article/287
- Common libvrt errors: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-troubleshooting-common_libvirt_errors_and_troubleshooting
- Netplan: https://netplan.readthedocs.io/en/stable/netplan-yaml/
- Kvm libvirt: https://libvirt.org/formatnetwork.html