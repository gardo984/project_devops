# Instalación

Glosario por el servicio **Jenkins** :
- Instalación y configuración de Jenkins.
	- [Manera Automatizada](#manera-automatizada).
    	- Requisitos Previos.
    	- Despliegue.
- [Notas Importantes](#notas-importantes).

## Manera Automatizada

### Requisitos Previos
- <a href="../ansible/playbooks#requisitos-previos" target="_blank">Instalación de Ansible</a>.
- <a href="../docker#manera-automatizada" target="_blank">Instalación de Docker</a>.
- Para mayores detalles de lo que es posible realizar con Ansible, favor de seguir el siguiente <a href="../ansible/playbooks#comandos-disponibles" target="_blank"><b>enlace<b></a>.

### Despliegue
- Instalación atravéz de ansible:
	- Ubicarse en el directorio de **playbooks**:
	```sh
	git clone -v "https://gitlab.com/gardo984/project_devops.git"
	cd ./documentation/jenkins/ansible/playbooks
	```
	- Ejecución del playbook:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml
	```

- Para configuraciones adiciones, favor de seguir el siguiente **[enlace](ansible/playbooks/docker-compose/jenkins#docker-jenkins)**.

## Notas Importantes

- Por algún proposito se necesite acceder a las plantillas de **Docker** utilizadas, se encuentran en la siguiente ruta:
```
documentation/jenkins/ansible/playbooks/docker-compose/jenkins/
```
- Para ejecución de las plantillas de **docker compose**:
    - Para entorno productivo:
    ```sh
    docker-compose -f compose_jenkins.yml  up -d
    ```
- Validación que los servicios se encuentren ejecutandose:
    - Para entorno productivo:
    ```sh
    docker-compose -f compose_jenkins.yml  ps
    ```
- Para la validación de servicios:
  - Estado **`jenkins`**:
  ```sh
  http://localhost:8080
  ```