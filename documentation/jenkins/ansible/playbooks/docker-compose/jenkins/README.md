
# Docker - Jenkins

El presente documento abarcará los siguientes puntos:
- Despliegue de Jenkins atravéz de docker

Glosario:
- [Pasos Previos](#pasos-previos)
- [Despliegue Jenkins](#despliegue-jenkins)
- [Configuración de Pipelines](#configuración-de-pipelines)

## Pasos Previos
Para el despliegue de Jenkins en un entorno contenerizado se requiere:
- Tener instalado <a href="../../../../../ansible/playbooks#requisitos-previos" target="_blank"><b>Ansible</b></a> de **Manera Local**.
- Tener instalado <a href="../../../../../docker#manera-automatizada" target="_blank"><b>Docker y Docker Compose</b></a> en el **Equipo Remoto**.

Sí ya cuenta con los requisitos previos, favor de omitir el presente paso.

## Despliegue Jenkins

Seguir los siguientes pasos:
- Ubicarse en el directorio **`documentation/jenkins/ansible/playbooks`**.
- Identificar el playbook relacionado a Jenkins `deploy_docker_jenkins.yaml`:
	- Listar Tasks / Procesos:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml --list-tasks
	```
	- Listar Tags, en este caso cada tag disponible identifica una operación:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml --list-tags
	```
	- Listar Tasks por Tag:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml -t deploy-service --list-tasks
	ansible-playbook deploy_docker_jenkins.yaml -t stop-service --list-tasks
	```
	- Desplegar Jenkins Contenerizado atravéz de ansible:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml -t deploy-service
	```
	- Detener Jenkins Contenerizado atravéz de ansible:
	```sh
	ansible-playbook deploy_docker_jenkins.yaml -t stop-service
	```
	- Intentar conectarse al host remoto por el puerto `8080`:
	```sh
	$ curl -I http://<remote-host>:8080/login
	HTTP/1.1 200 OK
	Date: Thu, 23 Sep 2021 15:59:10 GMT
	X-Content-Type-Options: nosniff
	Content-Type: text/html;charset=utf-8
	Expires: Thu, 01 Jan 1970 00:00:00 GMT
	Cache-Control: no-cache,no-store,must-revalidate
	X-Hudson: 1.395
	X-Jenkins: 2.313
	X-Jenkins-Session: ac6d30e6
	X-Frame-Options: sameorigin
	X-Instance-Identity: .....
	Set-Cookie: JSESSIONID.69de3f23=...; Path=/; HttpOnly
	Content-Length: 2025
	Server: Jetty(9.4.43.v20210629)
	```

## Configuración de Pipelines
Para la configuración de pipelines, favor de seguir el siguiente <a href="pipelines/" target="_blank"><b>enlace</b></a> .