
# Jenkins - ELK Pipelines

El presente documento abarca la siguientes tópicos:
- Creación de un Job / Tarea en Jenkins.
- Configuración de pipelines relacionados a:
	- Instalación y Desinstalación de Docker y Docker Compose.
	- Inicialización y Mantenimiento de cluster con Docker Swarm.
	- Despliegue de servicios ELK.

Glosario:
- [Pasos Previos](#pasos-previos)
- [Configuración de Pipeline](#configuración-del-pipeline)
- [Instalación de Docker, Docker Compose](#instalación-docker-y-docker-compose).
- [Desintalación de Docker, Docker Compose](#desintalación-docker-y-docker-compose).
- [Despliegue de Docker Swarm Cluster](#despliegue-docker-swarm).
- [Mantenimiento de Docker Swarm Cluster](#mantenimiento-docker-swarm):
	- Agregar Nodos (Manager, Worker).
	- Eliminar Nodos.
	- Estado de Nodos.
- [Despliegue servicios ELK](#despliegue-servicios-elk):
	- Cluster elasticsearch
	- Kibana
	- APM

## Pasos Previos
Para la configuración de pipelines se requiere disponer de un host y tener desplegado **`Jenkins`**:
- En caso ya se cuenta con uno, favor de omitir el presente paso.
- En caso no se cuente con un **`Jenkins`** desplegado, favor de seguir el siguiente <a href="../README.md" target="_blank"><b>enlace</b></a> para el despliegue.

## Configuración del Pipeline:
- Creación de una nuevo Job:

![Crear Job](images/jenkins-create-job.png)
- Tipo de Job **`Pipeline`**:

![Tipo Job](images/jenkins-config-job.png)
- Configuración de repositorio:

![Config Repo](images/jenkins-config-repo.png)
- Referenciar archivo **`groovy`**:

![Config Groovy](images/jenkins-config-groovy.png)

## Instalación Docker y Docker Compose
- Pipeline respectivo:

![Pipeline Install Docker](images/pipeline-install-docker.png)

## Desintalación Docker y Docker Compose
- Pipeline respectivo:

![Pipeline Install Docker](images/pipeline-uninstall-docker.png)
## Despliegue Docker Swarm
- Pipeline respectivo:

![Pipeline Install Docker](images/pipeline-setup-swarm.png)
## Mantenimiento Docker Swarm
- Pipeline respectivo:

![Pipeline Install Docker](images/pipeline-maintenance-swarm.png)
## Despliegue Servicios ELK
- Pipeline **Iniciar** Elastic:

![Pipeline Setup Elastic](images/pipeline-setup-elastic.png)
- Pipeline **Status** Elastic:

![Pipeline Status Elastic](images/pipeline-status-elastic.png)
- Pipeline **Stop** Elastic:

![Pipeline Stop Elastic](images/pipeline-stop-elastic.png)