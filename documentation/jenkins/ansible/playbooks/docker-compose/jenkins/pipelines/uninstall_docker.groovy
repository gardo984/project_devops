#!groovy
pipeline {
	agent any
	environment {
		DOCKERFILE_ANSIBLE_DIR = 'ansible/playbooks/docker-compose/jenkins/pipelines'
	}
	stages {
		stage("Task Execution"){
			agent {
				dockerfile {
		            filename "${env.DOCKERFILE_ANSIBLE_DIR}/Dockerfile.ansible"
		            dir "."
		            args '-u root'
		        }
			}
			steps {
				dir("ansible/playbooks") {
					sh "ansible-playbook uninstall_docker.yaml"
				}
			}
		}
	}
	post {
		success {
            sh "echo 'Processed Completed Successfully'"
        }
        failure {
            sh "echo 'Error, process failed'"
        }
        cleanup {
            deleteDir()
        }
	}
}
