
# NFS Server

Glosary:

- [Introduction](#introduction)
- [Installation](#installation)
- [Configuration](#configuration)
	- [Server Side](#server-side)
	- [Client Side](#client-side)
- [References](#references)

## Introduction

NFS is a native service used for sharing directories over the network between different hosts, its sharing policies fit well for complex and simple scenarios based on its requirements.

Some other services that matches some of the targets is Samba 4, but it comes with some additional improved features such as domain controller and audit logs.

## Installation

### Rocky Linux

- Install required package:
```sh
sudo yum install nfs-utils -y
```
- Enable and start main services:
```sh
systemctl enable nfs-server
systemctl enable rpcbind
systemctl start nfs-server
systemctl start rpcbind
```

### Ubuntu

- Install required package:
```sh
sudo apt-get install -y nfs-common libnfs-utils nfs-kernel-server
```
- Enable and start main services:
```sh
systemctl enable nfs-server
systemctl enable rpcbind
systemctl start nfs-server
systemctl start rpcbind
```

## Configuration

- Install the client tools:
	- **Ubuntu**:
	```sh
	sudod apt-get install nfs-common -y
	```
	- **CentOS / Rocky Linux**:
	```sh
	sudod yum install nfs-utils -y
	```

### Server Side

>#### Important
> If you are attempting to create a shared source to be integrated with kubernetes, take a look into the  property `no_root_squash` that might can be used in the following way:
> ```
> <path-to-share> <hostname>(rw,no_root_squash)
> ```

- Create the directory that will be shared over the network.
```sh
mkdir -p /mnt/nfs_share
```
- In order to share files / directories over the network, the file **`/etc/exports`** should be modified:
```txt
<directory> <host>(<options>)
```
```txt
/mnt/nfs_share 192.168.0.10(rw)
```
- Export (share) all the configurations:
```sh
exportfs -a
```
- Check the status of the shared files / directories:
```sh
exportfs -s
```
- To stop sharing a directory:
```sh
exportfs -u 192.168.0.10:/mnt/nfs_share
```

### Client Side

- Discover the sharing directories of a NFS server:
```sh
showmount -e 192.168.0.11
```
- To connect to shared directory:
```sh
sudo mount -t nfs 192.168.0.11:/mnt/nfs_share /mnt/nfs_server
```

## References

- NFS configuration: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/storage_administration_guide/nfs-serverconfig
- NFS utils commands: https://www.ibm.com/docs/en/aix/7.1?topic=e-exportfs-command