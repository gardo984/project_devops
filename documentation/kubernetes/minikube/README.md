# Minikube

The current file will explain the right steps to follow for installing k8s minikube.

## Installation

### Centos 7 / Rocky Linux

- For **`Docker`** installation, please follow the next **[link](../../docker#manera-automatizada)**:
- Download the package:
```sh
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```
- Start Cluster:
```sh
minikube start
```
- Enable Addons:
```sh
minikube addons enable portainer
minikube addons enable ingress
minikube addons enable ingress
minikube addons enable dashboard
minikube addons enable registry
minikube addons enable metrics-server
```
- Create an alias for **`kubectl`**:
	- Real time
	```sh
	alias kubectl="minikube kubectl --"
	```
	- Permanent configuration:
	```sh
	echo "alias kubectl=\"minikube kubectl --\"" >> /home/${USER}/.bashrc
	```
- Test **`kubectl`**:
```sh
kubectl get all -A
```

## References

- Minikube Start: https://minikube.sigs.k8s.io/docs/start/
- Install kubectl client: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/