# Kubernetes

Glosary of the topics discussed in the current repository:
- [Introduction](#introduction)
- [Core Objects](#core-objects)
	- [Namespace](#namespace)
	- [Pod](#pod)
	- [ReplicaSet](#replicaset)
	- [Deployment](#deployment)
	- [Service](#service)
	- [Jobs / CronJobs](#jobs-cronjobs)
	- [Ingress](#ingress)
- [Configuration Objects](#configuration-objects)
	- [Config](#config)
	- [Service Account](#service-account)
	- [ConfigMap](#configmap)
	- [Secret](#secret)
	- [Taints and Tolerations](#taints-and-tolerations)
	- [Labels and Node Affinity](#labels-and-node-affinity)
	- [Resource Requirements](#resource-requirements)
- [Observability](#observability)
	- [Readiness / Liveness](#readiness-liveness)
- [Create Objects](#create-objects)
- [Tips](#tips)
- [References](#references)

## Introduction

K8s is an orchestrator application for containers administration.

If you are interested to install k8s in your environment, please follow the next **[link](minikube#installation)**.

## Core Objects
- List nodes:
```sh
kubectl get nodes
```
- List all objects:
```sh
kubectl get all
```
- **Delete** operations:
	- Remove all pods with status `Terminating`:
	```sh
	kubectl delete pods --all -n <namespace> --grace-period 0 --force
	```
	- Remove **`Pod`**, **`ReplicaSet`** and **`Deployment`**:
	```sh
	kubectl delete pod <pod-name>
	kubectl delete rs <replicaset-name>
	kubectl delete deployment <deployment-name>
	```

### Namespace
- List namespaces:
```sh
kubectl get namespaces
```
- Create resource with a single command:
```sh
kubectl create ns <namespace>
```
- Set a namespace as a default:
```sh
kubectl set context $(kubectl config current-context) --namespace=<namespace>
```
- Get a template without creating the resource:
	- Execute the command:
	```sh
	kubectl create ns development --dry-run=client -o yaml
	```
	- Outcome:
	```yaml
	apiVersion: v1
	kind: Namespace
	metadata:
	  creationTimestamp: null
	  name: development
	spec: {}
	status: {}
	```

### Pod

- DNS name follows the pattern:
```sh
<service-name>.<namespace>.svc.cluster.local
```
- List:
```sh
kubectl get pods -o wide -n default
kubectl get pods -A
```
- Get pods count by namespace:
```sh
kubectl get pods -n <namespace> --no-headers | wc -l
```
- export yaml configuration:
```sh
kubectl get pod <pod-name> -o yaml > pod-configuration.yaml
```
- apply yaml changes to a running pod:
```sh
kubectl apply -f pod-configuration.yaml
```
- Create resource with a single command:
```sh
kubectl run <pod-name> --image=<image-name>
```
- Get a yaml template without creating the resource:
	- Execute command:
	```sh
	kubectl run pod-test --image=httpd --dry-run=client -o yaml
	```
	- Outcome:
	```yaml
	apiVersion: v1
	kind: Pod
	metadata:
	  creationTimestamp: null
	  labels:
	    run: pod-test
	  name: pod-test
	  namespace: <namespace>
	spec:
	  containers:
	  - image: httpd
	    name: pod-test
	    resources: {}
	  dnsPolicy: ClusterFirst
	  restartPolicy: Always
	status: {}
	```
- Create a pod and expose its port:
```sh
kubectl run <pod-name> --image=<image-name> --port=<port-number> --labels="label1=value1" --expose
```

### ReplicaSet

- List replicasets:
```sh
kubectl get replicaset
```
- Export **`ReplicaSet`** configuration in **yaml** format:
```sh
kubectl get replicaset <replicaset-name> -o yaml
```
- Change the **replicas** of a **`ReplicaSet`**:
	- Using **`scale`** option:
	```sh
	kubectl scale replicaset <replicaset-name> --replicas <number-value>
	```
	- Using **`edit`** option:
	```sh
	kubectl edit replicaset <replicaset-name>
	```

### Deployment

- Create:
```sh
kubectl create -f deployment-definition.yaml
```
- Get deployments:
```sh
kubectl get deployments
```
- Describe deployment:
```sh
kubectl describe deployment <deployment-name>
```
- Edit deployment:
```sh
kubectl edit deployment <deployment-name> --record=true
```
- Check status of a deployment:
```sh
kubectl rollout status deployment <deployment-name>
```
- Check history of a deployment:
```sh
kubectl rollout history deployment <deployment-name>
```
- Perform a rollback of a deployment:
```sh
kubectl rollout undo deployment <deployment-name>
```
- Set an image to a deployment:
```sh
kubectl set image deployment <deployment-name> <container-name>=<image-name>:<image-version>
```
- Export deployment configuration
```sh
kubectl get deployment <deployment-name> -o yaml
```
- Create resource with a single command:
```sh
kubectl create deployment test-deployment --replicas=<number> --image=<image>
```
- Get a yaml template without creating the resource:
	- Execute the command:
	```sh
	kubectl create deployment test-deployment --image=httpd --replicas=2 --dry-run=client -o yaml
	```
	- Outcome:
	```yaml
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  creationTimestamp: null
	  labels:
	    app: test-deployment
	  name: test-deployment
	spec:
	  replicas: 2
	  selector:
	    matchLabels:
	      app: test-deployment
	  strategy: {}
	  template:
	    metadata:
	      creationTimestamp: null
	      labels:
	        app: test-deployment
	    spec:
	      containers:
	      - image: httpd
	        name: httpd
	        resources: {}
	status: {}
	```

### Service
- List services:
```sh
kubectl get svc -n <namespace>
```
- Get a yaml template without creating the resource:
	- create service `NodePort` type:
		- this use `Service Name` as **selector**:
		```sh
		kubectl create svc nodeport <service-name> --tcp=<port>:<target-port> --node-port=<node-port> --dry-run=client -o yaml
		```
		- this use `Pod Name` as **selector** and the property `nodePort` can not be specified:
		```sh
		kubectl expose pod <pod-name> --name=<service-name> --port=<port> --type=NodePort dry-run=client -o yaml
		```
		- Outcome:
		```yaml
		apiVersion: v1
		kind: Service
		metadata:
		  creationTimestamp: null
		  labels:
		    app: <service-name> or <pod-name>
		  name: <service-name>
		spec:
		  ports:
		  - name: 80-80
		    nodePort: <node-port>
		    port: <port>
		    protocol: TCP
		    targetPort: <port>
		  selector:
		    app: <service-name> or <pod-name>
		  type: NodePort
		status:
		  loadBalancer: {}
		```
	- create service `ClusterIP` type:
		- this use `Pod Name` as **selector**:
		```sh
		kubectl expose pod <pod-name> --name=<service-name> --port=<port> --target-port=<target-port> dry-run=client -o yaml
		```
		- this use `Service Name` as **selector**:
		```sh
		kubectl create svc clusterip <service-name> --tcp=<port>:<target-port> --dry-run=client -o yaml
		```
		- Outcome:
		```yaml
		apiVersion: v1
		kind: Service
		metadata:
		  creationTimestamp: null
		  labels:
		    run: <service-name> or <pod-name>
		  name: <service-name>
		spec:
		  ports:
		  - port: <port>
		    protocol: TCP
		    targetPort: <target-port>
		  selector:
		    run: <service-name> or <pod-name>
		status:
		  loadBalancer: {}
		```

### Jobs / CronJobs

- List jobs and cronjobs:
```sh
kubectl get jobs,cronjobs
```
- Create a **`Job`** in a imperative way:
```sh
kubectl create job job-hello-world --image=httpd --dry-run=client -o yaml -- echo -n "hello world"
```
- Create a **`CronJob`** in a imperative way:
```sh
kubectl create cronjob cron-job-hello-world --image=httpd --dry-run=client -o yaml --schedule="0 9 * * 1-5" -- echo -n "hello world"
```
- Job structure:
```yaml
apiVersion: batch/v1
kind: Job
metadata:
 name: job-calculate-sum
spec:
 completions: 3
 parallelism: 3
 template:
  spec:
   containers:
   - name: calculate-sum
     image: ubuntu
     command: ["expr", "2", "+", "3"]
   restartPolicy: Never
```
- CronJob structure:
```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
 name: cron-job-calculate-sum
spec:
 schedule: "0 8 * * 1-5"
 jobTemplate:
  spec:
    completions: 3
    parallelism: 3
    template:
     spec:
      containers:
      - name: calculate-sum
        image: ubuntu
        command: ["expr", "2", "+", "3"]
      restartPolicy: Never
```

### Ingress

- List all ingress objects:
```sh
kubect get ingress -A
```
- List all ingressclass available in your cluster:
```sh
kubectl get ingressclass -A
```
- Check ingress yaml structure definition:
```sh
kubectl explain ingress --recursive
```
- Create an ingress in a imperative way:
	- Bound to a hostname:
	```sh
	kubectl create ingress apache --class=default  --default-backend="apache:80" --rule=<hostname>/apache=apache:80 --dry-run=client -o yaml
	```
	- Unbound to a hostname:
	```sh
	kubectl create ingress apache --class=default  --default-backend="apache:80" --rule=/apache=apache:80 --dry-run=client -o yaml
	```
- A full example how to deploy an application with a **`Deployment`**, **`Service`** and an **`Ingress`** can be found in the directory **`specs/apache-ingress-deploy/`**:
	- Create all the objects:
	```sh
	kubectl create -f apache-deployment-definition.yaml
	kubectl create -f apache-service-definition.yaml
	kubectl create -f apache-ingress-definition.yaml
	kubectl get pods,svc,deployment,ingress -o wide
	```
	- Test the ingress configuration, an outcome like the following should be displayed:
	```sh
	$ curl -I http://<hostname>/apache
	HTTP/1.1 200 OK
	Date: Tue, 01 Feb 2022 17:12:38 GMT
	Content-Type: text/html
	Content-Length: 45
	Connection: keep-alive
	Last-Modified: Mon, 11 Jun 2007 18:53:14 GMT
	ETag: "2d-432a5e4a73a80"
	Accept-Ranges: bytes
	```

### Network Policies



## Configuration Objects

### Config


- To check the current configuration:
	- default way:
	```sh
	kubectl config view
	```
	- with a different configuration file:
	```sh
	kubectl config --kubeconfig=~/.kube/config view
	```
- To get clusters:
```sh
kubectl config get-clusters
```
- To get users:
```sh
kubectl config get-users
```
- To get contexts:
```sh
kubectl config get-contexts
```
- Config cluster:
	- Set domain and cert file:
	```sh
	kubectl config set-cluster <cluster-name> \
				--server=https://<site>:<port>
				--certificate-authority=<cert-path> \
      	--embed-certs=true
	```
	- Set `--insecure-skip-tls-verify`:
	```sh
	kubectl config set-cluster <cluster-name> --insecure-skip-tls-verify=true
	```
- Config account:
	- Set account and token:
	```sh
	kubectl config set-credentials <account-name> --token <account-token>
	```
- Config context:
	- To get current context:
	```sh
	kubectl config current-context
	```
	- Set user and server to a specific context:
	```sh
	kubectl config set-context <context-name> \
				--user=<account-name> \
				--server=<cluster-name> \
				--namespace=<namespace>
	```
	- Set context to use:
	```sh
	kubectl config use-context <context-name>
	```

### Service Account

- List **`Service Account`**:
```sh
kubect get sa
```
- Get information of a **`Service Account`**:
```sh
kubect get sa <service-account-name>
```
- Every **`Service Account`** is bound to a secret:
```sh
kubectl describe secret $(kubectl describe sa <service-account-name> |grep -i tokens |awk '{print $2}')
```

### ConfigMap
- Create a **`ConfigMap`**:
	- command:
	```sh
	kubectl create cm test --from-literal=foo=bar --dry-run=client -o yaml
	```
	- outcome:
	```yaml
	apiVersion: v1
	data:
	  foo: bar
	kind: ConfigMap
	metadata:
	  creationTimestamp: null
	  name: test
	```
- Inject **`ConfigMap`** to a pod:
```yaml
spec:
 containers:
  envFrom:
   mapConfigRef:
   - name: <mapconfig-name>
```
- List all objects:
```sh
kubectl get cm -A
```
- Describe a specific object:
```sh
kubectl describe cm <configmap-name>
```

### Secret

- Create a **`Secret`**:
	- command:
	```sh
	kubectl create secret generic test --from-literal=foo=bar --dry-run=client -o yaml
	```
	- outcome:
	```yaml
	apiVersion: v1
	data:
	  foo: YmFy
	kind: Secret
	metadata:
	  creationTimestamp: null
	  name: test
	```
- Inject **`Secret`** to a pod:
```yaml
spec:
 containers:
  envFrom:
   secretRef:
   - name: <secret-name>
```
- Encrypt values to **base64**:
```sh
echo -n "value" | base64
-> <encrypted-value>
```
- Decrypt values from **base64**:
```sh
echo -n "value" | base64 -d
-> <decrypted-value>
```
- List all objects:
```sh
kubectl get secrets -A
```
- Describe a specific object :
```sh
kubectl describe secret <secret-name>
```
### Taints and Tolerations

- Get nodes:
```sh
kubectl get nodes
```
- Taint a **`Node`**:
```sh
kubectl taint node <node-name> color=blue:NoSchedule --overwrite=true
```
- Check taints of a specific node:
```sh
kubectl describe node <node-name> |grep -i color
-> Taints: color=blue:NoSchedule
```
- Apply tolerations to a **`Pod`**:
```yaml
spec:
 tolerations:
 - key: "<key>"
   operator: "Equal"
   value: "<value>"
   effect: "<taint-effect>"
```
- Delete a taint on a specific **`Node`**, take a look the dash that is at end of the sentence **`-`**:
```sh
kubectl taint node <node-name> <key>=<value>:<taint-effect>-
```

### Labels and Node Affinity

- Create labels on a **`Node`**:
```sh
kubectl label node <node-name> foo=bar
```
- Check labels for **`Node`**:
```sh
kubectl get nodes --show-labels
# or
kubectl describe node <node-name>
```
- Use of **`Node Selector`** and **`Node Affinity`** on `Pods`:
	- Node Selector:
	```yaml
	spec:
	 nodeSelector:
      foo: bar
	```
	- Node Affinity:
		- Type **`requiredDuringSchedulingIgnoredDuringExecution`**:
		```yaml
		spec:
		  affinity:
		   nodeAffinity:
		    requiredDuringSchedulingIgnoredDuringExecution:
		     nodeSelectorTerms:
		     - matchExpressions:
		       - key: "foo"
		         operator: "In"
		         values:
		         - "bar"
		```
		- Type **`preferredDuringSchedulingIgnoredDuringExecution`**:
		```yaml
		spec:
		  affinity:
		   nodeAffinity:
		    preferredDuringSchedulingIgnoredDuringExecution:
		     - weight: 1
		       preference:
		        matchExpressions:
		         - key: "foo"
		           operator: "In"
		           values:
		           - "bar"
		```


### Security Context
- To identify all available options:
```sh
kubectl explain pods --recursive |grep -i "securityContext" -A20
```
- Specify `Security Context` on:
	- Container:
	```yaml
	spec:
	 containers:
	  securityContext:
	   runAsUser: 0
	```
	- Pod
	```yaml
	spec:
	 containers:
	  ...
	  ...
	 securityContext:
	   runAsUser: 0
	```
- To add **`Linux capabilities`** through **`Security Context`**, please check the following **[document](https://unofficial-kubernetes.readthedocs.io/en/latest/concepts/policy/container-capabilities/)**.
```yaml
spec:
 containers:
  ...
  ...
  securityContext:
   runAsUser: 0
   capabilities:
    add:
    - CAP_KILL
```
- For additional details try to take a look in man pages, otherwise **[here](https://man7.org/linux/man-pages/man7/capabilities.7.html)** official documentation:
```sh
man -k capabilities
```
### Resource Requirements

- Add resource requirements to a **`Pod`**:
```yaml
spec:
 containers:
  - resources:
    requests:
     cpu: 0.25
     memory: "500Mi"
    limits:
     cpu: 0.25
     memory: "1Gi"
```

## Observability

### Readiness / Liveness

- Readiness and Liveness can be apply in 3 kinda ways:
	- Web url test
	```yaml
	readinessProbe:
      httpGet:
        path: /
        port: 80
      initialDelaySeconds: 10
      periodSeconds: 5
      failureThreshold: 4
    livenessProbe:
      httpGet:
        path: /
        port: 80
      initialDelaySeconds: 10
      periodSeconds: 5
      failureThreshold: 4
	```
	- Socket test
	```yaml
	readinessProbe:
	 tcpSocket:
	  port: 3306
	livenessProbe:
	 tcpSocket:
	  port: 3306
	```
	- Command test
	```yaml
	readinessProbe:
      exec:
        command:
        - curl
        - -i
        - http://localhost:80/
      initialDelaySeconds: 10
    livenessProbe:
      exec:
        command:
        - curl
        - -i
        - http://localhost:80/
      initialDelaySeconds: 10
	```
- example applying readiness and liveness with `Apache Service`:
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: apache
  name: apache
spec:
  containers:
  - image: pisckitama/httpd-curl
    name: apache
    ports:
    - containerPort: 80
    resources: {}
    readinessProbe:
      httpGet:
        path: /
        port: 80
      initialDelaySeconds: 10
      periodSeconds: 5
      failureThreshold: 4
    livenessProbe:
      exec:
        command:
        - curl
        - -i
        - http://localhost:80/
      initialDelaySeconds: 10

  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

## Create Objects
- Configuration for a **`Pod`**:
```sh
kubectl create -f pod-definition.yaml
```
- Configuration for a **`ReplicaSet`**:
```sh
kubectl create -f replicaset-definition.yaml
```
- Configuration for a **`Deployment`**:
```sh
kubectl create -f deployment-definition.yaml
```

## Tips

- To remember the yaml syntax for a specific **`Object`**, perform the following command:
```sh
kubectl explain pods --recursive |less
```
- Some similarities regarding **`Command`** and **`Arguments`** on **Docker** and **Kubernetes**:
	- Docker:
	```dockerfile
	ENTRYPOINT ["/bin/echo"]
	CMD ["world"]
	```
	- Kubernetes:
	```yaml
	spec:
	 containers:
	 - name: apache
	   image: httpd
	   command: ["/bin/echo"]
	   args:
	   - "world"
	```

- To create an account role and access your cluster remotely, execute the following script *`specs/create_account.sh`*, it will create the following resources:
	- serviceaccount resource.
	- secret resource (bound to serviceaccount).
	- clusterrolebinding resource.
	- finally, a kubeconfig file to be configured on your box and access your cluster remotely.
	```sh
	$ sh ./documentation/kubernetes/specs/create_account.sh
	Please enter a username for the account to be created:                                                                                         
	<username-to-be-created>
	```

## References

- Kubectl reference docs: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-
- Ingress information: https://kubernetes.io/docs/concepts/services-networking/ingress/
- Service information: https://kubernetes.io/es/docs/concepts/services-networking/service/
- Nginx ingress: https://kubernetes.github.io/ingress-nginx/deploy/
- Security in secrets: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
- Capability Similarities between Docker and K8s: https://unofficial-kubernetes.readthedocs.io/en/latest/concepts/policy/container-capabilities/
- Documentation about capabilities: https://man7.org/linux/man-pages/man7/capabilities.7.html
- Readiness and Liveness: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/
- Information related to ingress controllers:
	- https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-ingress-em- 
	- https://kubernetes.io/docs/concepts/services-networking/ingress
	- https://github.com/kubernetes/ingress-nginx/blob/main/docs/examples/rewrite/README.md
	- https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/annotations/
- Information related to Persistent Volume:
	- https://kubernetes.io/docs/concepts/storage/volumes/
	- https://kubernetes.io/docs/concepts/storage/storage-classes
	- https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/
	- https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner
	- https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/#create-a-persistentvolume
- K8s cluster installation: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
- K8s configure remote access: https://faun.pub/manually-connect-to-your-kubernetes-cluster-from-the-outside-d852346a7f0a
- Service account configuration:
	- https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/
	- https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/#manually-create-a-service-account-api-token
	- https://kubernetes.io/docs/reference/access-authn-authz/rbac/#service-account-permissions