
# Kustomize

Glossary:
- [Introduction](#introduction)
- [Use](#use)
	- [Declarative](#declarative)
	- [Imperative](#imperative)

## Introduction

Kustomize is a CLI tool that will help to create k8s resources in a easy way for different environments.

## Use

- Create a directory (lets call it `example-app`) and a file inside it called `kustomization.yaml` and specify your k8s resources (for this example we have a deployment and a service), such the following:
```yaml
# k8s resources to be managed by kustomize
resources:
  - deployment-backend.yaml
  - service-backend.yaml


# Customizations that need to be made
commonLabels:
  company: penguin-knowledge
```

- To generate/build the final state of the resources (an outcome will be displayed):
```sh
kustomize build example-app/
```

- To create the resources on a k8s cluster:
```sh
kustomize build example-app/ | kubectl apply -f -
# or
kubectl apply -k example-app/
```

- To remove the resources on a k8s cluster:
```sh
kustomize build example-app/ | kubectl delete -f -
# or
kubectl delete -k example-app/
```

- To remove all unused secrets or configMaps:
```sh
kubectl apply -k example-app/ --prune -l <label-name>=<label-value>
```

### Declarative

For declarative examples, take a look into all `kustomization.yaml` files into the `example-app` dir:
```sh
find example-app/ -type f -iname "kustomization.yaml"
```

### Imperative

- Set namespace:
```sh
kustomize edit set namespace production
```
- Set common labels:
```sh
kustomize edit set label role:admin org:kodekloud
```
- Remove common labels:
```sh
kustomize edit remove label role
kustomize edit remove label org
```
- Replace an image for a new one:
```sh
kustomize edit set image nginx=memcached
```
- Update image for a specific `Deployment`:
```sh
kustomize edit set replicas api-deployment=8
```
- Add a new resource file to `kustomization.yaml` file:
```sh
kustomize edit add resource <path-file> 
```

- Create configmap generator on `kustomization.yaml` file:
```sh
kustomize edit add configmap <configmap-name> --from-literal=<key-name>=<key-value>
cat <<EOF >> kustomization.yaml
configMapGenerator:
- name: domain-config
  literals:
  - key1=value1
  - key2=value2
EOF

# or

kustomize edit add configmap <my-configmap> --from-literal="key1=value1"
```

- Create secret generator on `kustomization.yaml` file:
```sh
kustomize edit add secret <secret-name> --from-literal=<key-name>=<key-value>
cat <<EOF >> kustomization.yaml
secretGenerator:
- name: my-secret
  literals:
  - key1=value1
  - key2=value2
EOF

# or

kustomize edit add secret <my-secret> --from-literal="key1=value1"
```