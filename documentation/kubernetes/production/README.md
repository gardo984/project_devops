# K8s Installation

The current document will contain details for a deployment on a **production environment**.

Glossary:

- [Configuration](#configuration)
- [Automated Way](#automated-way)
- [Manual Way](#manual-way)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Cluster Setup](#cluster-setup)
  - [Join Nodes](#join-nodes)
- [References](#references)

## Configuration

A kubernetes cluster is managed by a **master node** (well known as `control plane`) and **worker node**:

- Master node:
    - **kube-controller-manager**: Interacts with the `kube-api-server` to determine the state of the cluster, if the state does not match (with the declared one requested to the `kube-api-server`), the manager will contact the required controllers to match the desired state.
    - **kube-scheduler** : see the API requests for running a new container and look for a suitable node to run it (according to metrics, resources, tolerations, labels, etc).
    - **kube-api-server** : main interface for queries and updates related to objects  until the declared state matches with the current one.
        - Authentication
        - Authorization
        - Admission Controllers (constraints defined on a cluster / namespace)
    - **etcd datastore**:
        - container settings
        - networking configuration
        - state of the cluster
    - **kubelet** -> containerd
    - **kube-proxy** -> iptables / eBPF
    - **cloud-controller-manager** (depends of the platform: Gcp, Aws, Azure): handles tasks for third party integrations (management or reporting) such as Digital Ocean, Rancher, etc.
- Workers:
    - **kubelet**: receives spec information for a container configuration, downloads and manages any necessary resources (deploy, service, secret, etc) and finally, works with the container engine to run it or is restarted upon failure.
        - containerd
    - **kube-proxy**: manage and creates local firewall rules and network configuration to expose containers on the network.
        - iptables / eBPF

![Kubernetes Architecture](./resources/k8s-architecture.jpeg)

Orchestration is based on operators or controllers such as:

- **Deployments** (new one)
    - **ReplicaSets**
        - **Pod**
- **Jobs**
- **CronJobs**
- **Custom Resource Definition**
- **purpose-built operators**
- **DeamonSet**: ensure a Pod is deployed in every node (often used for logging, metrics, security).
- **StatefulSet**: ensure to deploy a pod in specific order based on Pod status(health checks).

Controllers or Operators can manage:

- **Labels** (selectors)
- **Object Metadata**: Often used on Pods or Nodes (taints that are arbitrary strings).
    - **Annotations** : it is part of an object but can not be used as selectors.

Kubernetes fits good in a multi-tenancy architecture:

- Users to have its own cluster.
- Users and groups to access to one or more clusters.

To achieve the kinda isolation in a multi-tenant cluster, k8s uses the following resources:

- **Namespaces**: A way to segregate resources and is also possible to define controller constraints resource usage (k8s objects can be created into namespace or cluster-scoped).
- **Context**: A combination of cluster and user that is configured in the kube config file `~/.kube/config`.
- **Resource Limits**: A way to limit the resources consumed by a pod, this feature can be set to namespaces too.
- **Pod Security Policies**: Deprecated (replacement `Pod Security Admission`).
- **Pod Security Admission**: Restrict pod behavior.
- **Network Policies**: The ability to have a firewall inside the cluster and control the ingress and egress traffic according to namespaces, labels as well as networks traffic characteristics.


Kubernetes Add-ons (third party solutions):

- **DNS services**
- **Logging**
- **Monitoring**


## Automated Way



## Manual Way

### Prerequisites

- Disable **selinux**:
```sh
setenforce 0
sed -i -E 's/^SELINUX=(enforcing|permissive)$/SELINUX=disabled/g' /etc/selinux/config
```
- Disable **firewalld**:
```sh
systemctl disable firewalld
systemctl stop firewalld
```
- Add hostname and ip in the hosts file **`/etc/hosts`**:
```sh
cat <<EOF |tee -a /etc/hosts
<host-ip> <hostname>
EOF
```
- Load `kernel modules` in persistent and realtime way:
```sh
cat <<EOF | tee -a /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

modprobe overlay
modprobe br_netfilter
```
- Set `kernel` configurations and reload kernel configs:
```sh
cat <<EOF | tee -a /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

sysctl --system
```
- Add docker repositories and install container runtime interface (**`containerd`**):
```sh
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
yum install -y containerd.io
```
- Apply some configurations on containerd config file **`/etc/containerd/config.toml`**:
```sh
containerd config default > /etc/containerd/config.toml
sed -i -E 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
```
- Restart `containerd` service:
```sh
systemctl enable containerd
systemctl restart containerd
systemctl status containerd
```
- Validate that the module **`br_netfilter`** is loaded into kernel modules:
```sh
lsmod |grep -i br_netfilter
```
- Download and install **`runc`** binary file based on the OS arch:
```sh
arch=$([ $(uname -p) == "aarch64" ] && echo "arm64" || echo "$(uname -p)")
wget -v "https://github.com/opencontainers/runc/releases/download/v1.1.3/runc.${arch}"
install -m 755 runc.$arch /usr/local/sbin/runc
```
- Download and install `CNI` plugins based on **OS** + **arch** + **version**:
```sh
arch=$([ $(uname -p) == "aarch64" ] && echo "arm64" || echo "$(uname -p)")
wget -v "https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-${arch}-v1.1.1.tgz"
mkdir -p /opt/cni/bin
tar Cxzvf /opt/cni/bin "cni-plugins-linux-${arch}-v1.1.1.tgz"
```
- Restart containerd service:
```sh
systemctl restart containerd
```

### Installation

- Add a repository:
```sh
cat <<EOF | tee -a /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
```
- Install **`K8s`** and enable the service:
```sh
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable --now kubelet
```

### Cluster Setup

- Download the images:
```sh
kubeadm config images list
kubeadm config images pull
```
- Initialize the cluster, the value of **`--pod-network-cidr`** can be any available range:
> #### Important!
> After setting up the cluster, if you are running a single cluster, you will need to untaint the master node in order to run containers on it:
> ```sh
> kubectl taint nodes <hostname> node-role.kubernetes.io/master-
> kubectl taint nodes <hostname> node-role.kubernetes.io/control-plane-
> ```
```sh
kubeadm init  --control-plane-endpoint <host-ip> --pod-network-cidr=10.10.0.0/16
```
- Validate that `kubelet` property **`cgroupDriver`** is correctly configured with the value of **`systemd`**:
```sh
$ cat /var/lib/kubelet/config.yaml  |grep -i cgroupDriver
cgroupDriver: systemd
```
- Set the kube config variable:
> #### Important!
> If you want to grant access to other users, perform the following operation:
> ```sh
> account="<account-name>"
> mkdir -p "/home/$account/.kube" && \
>    cp -avR /etc/kubernetes/admin.conf "/home/$account/.kube/config" && \
>    chown $account:$account -R "/home/$account/.kube"
> ```
```sh
export KUBECONFIG=/etc/kubernetes/admin.conf
```
- Install Calico **`CNI`** (there are other available in the networking addons url):
```sh
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```
- Validate the state of the cluster:
```sh
kubectl get nodes -o wide
```
### Join Nodes

- To join nodes to the cluster:
> #### Important!
> - To get the token (**take into consideration that tokens last an hour, in case it does not exist, you can create one**):
>   - To get tokens:
>   ```sh
>   kubeadm token list -o jsonpath="{.token}"
>   ```
>   - To create a token:
>   ```sh
>   kubeadm create token
>   ```
> - To get the `hostname` and `port`:
> ```sh
> kubectl get ep
> ```
> - To get the value of the parameter `--discovery-token-ca-cert-hash`:
> ```sh
> openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt \
>   | openssl rsa -pubin -outform der 2>/dev/null \
>   | openssl dgst -sha256 -hex | sed 's/^.* //'
> ```
```sh
kubeadm join \
  --token <host-token> <hostname>:<host-port> \
  --discovery-token-ca-cert-hash sha256:<hash>
```

## References

- Installation k8s Rocky linux: https://www.golinuxcloud.com/deploy-multi-node-k8s-cluster-rocky-linux-8/
- Containerd helpful links:
  - Installation : https://docs.docker.com/engine/install/centos/#install-docker-engine
  - K8s CRI prerequisites: https://kubernetes.io/docs/setup/production-environment/container-runtimes/#install-and-configure-prerequisites
  - Containerd getting started: https://github.com/containerd/containerd/blob/main/docs/getting-started.md
- Install kubeadmin: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
- Links for K8s production: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/
- CNI plugins for k8s:
  - Calico: https://projectcalico.docs.tigera.io/getting-started/kubernetes/quickstart
  - Networking addons: https://kubernetes.io/docs/concepts/cluster-administration/networking/
- Available addons: https://kubernetes.io//docs/concepts/cluster-administration/addons/

