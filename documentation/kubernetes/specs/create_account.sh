#!/bin/bash

echo "Please enter a username for the account to be created (if empty generic value generated):"
read USERNAME
echo "Please enter context name (if empty generic value generated):"
read TMP_CONTEXTNAME

HASH=$(openssl rand -hex 6)
HOSTNAME=$(hostname)

if [ -z $USERNAME ]; then
  USERNAME="dev-${HASH}"
fi

if [ -z $TMP_CONTEXTNAME ]; then
  TMP_CONTEXTNAME="${HOSTNAME}-${HASH}"
fi

ACCOUNT=${USERNAME}
CLUSTERNAME="${HOSTNAME}"
CONTEXTNAME="${TMP_CONTEXTNAME}"
DEFAULT_NAMESPACE='default'
PATH_SECRET_YAML="/tmp/secret-tmp-${HASH}.yaml"
PATH_CRT="/tmp/user-tmp-${HASH}.crt"
PATH_KUBECONFIG="/tmp/kubeconfig-tmp-${HASH}"

cat <<EOF> ${PATH_SECRET_YAML}
apiVersion: v1
kind: Secret
metadata:
  name: my-${ACCOUNT}-secret
  annotations:
    kubernetes.io/service-account.name: ${ACCOUNT}
type: kubernetes.io/service-account-token
EOF

cat <<EOF> ${PATH_KUBECONFIG}
apiVersion: v1
kind: Config
clusters:
- cluster:
  name: ${CLUSTERNAME}
users:
- name: ${ACCOUNT}
contexts:
- contexts:
  name: ${CONTEXTNAME}
EOF

kubectl create sa ${ACCOUNT}
kubectl create clusterrolebinding ${ACCOUNT} \
        --clusterrole cluster-admin \
        --serviceaccount ${DEFAULT_NAMESPACE}:${ACCOUNT}
kubectl create -f ${PATH_SECRET_YAML}

ACCOUNT_SECRET_NAME=$(kubectl get secret -o jsonpath="{.items[?(@['metadata.annotations.kubernetes\.io/service-account\.name']=='${ACCOUNT}')].metadata.name}")
KUBE_API_EP=$(kubectl get endpoints -o jsonpath='{range .items[?(@.metadata.name=="kubernetes")]}{.subsets[0].addresses[0].ip}{":"}{.subsets[0].ports[0].port}{"\n"}{end}')
KUBE_API_CERT=$(kubectl get secrets ${ACCOUNT_SECRET_NAME} -o jsonpath='{.data.ca\.crt}' | base64 --decode)
KUBE_API_TOKEN=$(kubectl get secrets ${ACCOUNT_SECRET_NAME} -o jsonpath='{.data.token}' | base64 --decode)
echo $KUBE_API_CERT > ${PATH_CRT}

kubectl config --kubeconfig=${PATH_KUBECONFIG} set-cluster ${CLUSTERNAME} \
      --server=https://${KUBE_API_EP} \
      --certificate-authority=${PATH_CRT} \
      --embed-certs=true \

kubectl config --kubeconfig=${PATH_KUBECONFIG} set-cluster ${CLUSTERNAME} --insecure-skip-tls-verify=true
kubectl config --kubeconfig=${PATH_KUBECONFIG} set-credentials ${ACCOUNT} --token ${KUBE_API_TOKEN}
kubectl config --kubeconfig=${PATH_KUBECONFIG} set-context ${CONTEXTNAME} \
        --user=${ACCOUNT} \
        --cluster=${CLUSTERNAME} \
        --namespace=${DEFAULT_NAMESPACE}

kubectl config --kubeconfig=${PATH_KUBECONFIG} use-context ${CONTEXTNAME}

printf "\nA kube config file has been generated: ${PATH_KUBECONFIG}\n"
printf "Copy the file or its content:\n\n"
cat ${PATH_KUBECONFIG}
