
# Fail2ban

The current file will contain information regarding the following points:
- [Installation](#installation)
	- [Centos 7 / Rocky Linux](#centos-7-rocky-linux)
- [Configuration](#configuration)
- [Useful Commands](#useful-commands)
- [References](#references)

## Installation

### Centos 7 / Rocky Linux
- Previous steps:
```sh
sudo systemctl stop firewalld
sudo systemctl disable firewalld
sudo sed -i -e 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
sudo setenforce 0
```
- If is possible, restart your system:
```sh
sudo init 6
```
- Install the **`epel-release`**:
```sh
sudo yum install -y epel-release
```
- Install **`fail2ban`** package and enable the service:
```sh
sudo yum install -y --nogpgcheck fail2ban
sudo systemctl enable fail2ban
```

### Ubuntu

- Previous steps:
```sh
sudo systemctl stop ufw.service
sudo systemctl disable ufw.service
```
- Install **`fail2ban`** package and enable the service:
```sh
sudo apt-get install -y fail2ban
sudo systemctl enable fail2ban
```

## Configuration

>>>
###### Important

In order to manage the blocked ip(s) in plain text, the **`Data persistent functionality`** has been disabled. In case you are interested to enable the functionality of the **`dbfile`**, you should set the value of **`FILE`** and all operations such as **`list`**, **`ban ip`**, **`others`** will be managed by the command **`fail2ban-client`**.
>>>


The following functionalities will be enabled:
- Ssh module.
- Permanent IP Ban.

Changes to be performed:
- Set the following values in the file **`/etc/fail2ban/fail2ban.conf`**:
```conf
# Disable persistent data functionality
dbfile = None
```
- Create the file **`/etc/fail2ban/jail.local`** and paste the following content:
```conf
[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
# values: -1 means permanent ban
bantime = -1
maxretry = 3
```
- Create the file **`/etc/fail2ban/persistent.bans`**:
```sh
touch /etc/fail2ban/persistent.bans 
```
- Modify the file **`/etc/fail2ban/action.d/iptables-multiport.conf`**:
```conf
# Fail2Ban configuration file
#
# Author: Cyril Jaquier
# Modified by Yaroslav Halchenko for multiport banning
#

[INCLUDES]

before = iptables-common.conf

[Definition]

# Option:  actionstart
# Notes.:  command executed on demand at the first ban (or at the start of Fail2Ban if actionstart_on_demand is set to false).
# Values:  CMD
#
actionstart = <iptables> -N f2b-<name>
              <iptables> -A f2b-<name> -j <returntype>
              <iptables> -I <chain> -p <protocol> -m multiport --dports <port> -j f2b-<name>
              cat <persistentfile>| uniq | awk '/^f2b-<name>/ {print $2}' \
                | while read IP; do iptables -I f2b-<name> 1 -s $IP -j <blocktype>; done

actionstart_on_demand = false

# Option:  actionstop
# Notes.:  command executed at the stop of jail (or at the end of Fail2Ban)
# Values:  CMD
#
actionstop = <iptables> -D <chain> -p <protocol> -m multiport --dports <port> -j f2b-<name>
             <actionflush>
             <iptables> -X f2b-<name>

# Option:  actioncheck
# Notes.:  command executed once before each actionban command
# Values:  CMD
#
actioncheck = <iptables> -n -L <chain> | grep -q 'f2b-<name>[ \t]'

# Option:  actionban
# Notes.:  command executed when banning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionban = <iptables> -I f2b-<name> 1 -s <ip> -j <blocktype> && \
            echo "f2b-<name> <ip>" >> <persistentfile>

# Option:  actionunban
# Notes.:  command executed when unbanning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionunban = <iptables> -D f2b-<name> -s <ip> -j <blocktype>

[Init]
persistentfile = /etc/fail2ban/persistent.bans

[Init?family=inet6]
persistentfile = /etc/fail2ban/persistent.bans

```
- Start service:
```sh
systemctl start fail2ban
systemctl status fail2ban
```

## Useful Commands

- Operations with **`iptables`**:
	- List all nats and chains:
	```sh
	iptables -L
	iptables -L <chain> -v -n --line-numbers
	```
	- Remove an IP:
	```sh
	iptables -D <chain> <position>
	```
	- List commands performed by **`fail2ban`** on **`iptables`**:
	```sh
	iptables -S <chain>
	```
- Operations with **`fail2ban-client`** and **`fail2ban-server`**:
	- Check status:
	```sh
	fail2ban-client status
	```
	- Check banned ips:
	```sh
	fail2ban-client banned
	```
	- Check version:
	```sh
	fail2ban-server version
	```


## References

- unban ip : https://bobcares.com/blog/fail2ban-unban-ip/
- fail2ban jail information: https://manpages.debian.org/experimental/fail2ban/jail.conf.5.en.html
- **`actionstart_on_demand`** issue : https://github.com/fail2ban/fail2ban/issues/2804
- examples and info regarding fail2ban on plesk: https://www.plesk.com/blog/various/using-fail2ban-to-secure-your-server/
- install fail2ban centos 7: https://www.digitalocean.com/community/tutorials/how-to-protect-ssh-with-fail2ban-on-centos-7
- configure permanent ban: https://arno0x0x.wordpress.com/2015/12/30/fail2ban-permanent-persistent-bans/
- fail2ban information: https://www.digitalocean.com/community/tutorials/how-fail2ban-works-to-protect-services-on-a-linux-server