# Instalación

Glosario por el servicio **Docker** y **Docker Compose** :
- Instalación y configuración de docker & docker compose.
	- [Manera Automatizada](#manera-automatizada).
		- Requisitos Previos.
		- Despliegue.
	- [Manera Detallada](#manera-detallada).

## Manera Automatizada

### Requisitos Previos
- <a href="../ansible/playbooks#requisitos-previos" target="_blank">Instalación de Ansible</a>.
- Para mayores detalles de lo que es posible realizar con Ansible, favor de seguir el siguiente <a href="../ansible/playbooks#comandos-disponibles" target="_blank"><b>enlace<b></a>.

### Despliegue
- Instalación atravéz de ansible:
	- Ubicarse en el directorio de **playbooks**:
	```sh
	git clone -v "https://gitlab.com/gardo984/project_devops.git"
	cd ./documentation/docker/ansible/playbooks
	```
	- Ejecución del playbook:
	```sh
	ansible-playbook install_docker.yaml
	```

- Obtener la versión de docker instalada como confirmación de instalación:
```sh
ansible-playbook -t test-docker-version tests.yaml
```

## Manera Detallada
- Paquetes previos a instalación de docker:
	```sh
	sudo yum install -y yum-utils net-tools lvm2 device-mapper-persistent-data
	```
- Instalación de **Docker**
	- Agregar el repositorio
	```sh
	sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	```
	- Si se desea visualizar las versiones de docker disponibles:
	```sh
	yum list docker-ce --showduplicates | sort -r
	```
	- Para esta oportunidad se optó por versión **`19.03.9`**:
	```sh
	sudo yum install -y docker-ce-19.03.9 docker-ce-cli-19.03.9 containerd.io
	```
	- Habilitar e iniciar los siguientes servicios:
	```sh
	systemctl status docker
	systemctl enable docker && systemctl start docker
	systemctl status containerd.service
	systemctl enable containerd.service && systemctl start containerd.service
	```
	- Validar que docker se encuentre instalado de manera correcta:
	```sh
	docker run hello-world
	```

- Configuración de docker
	- Ejecutar los siguientes comandos para sesion activa:
	```sh
	sudo swapoff -a
	sudo sysctl -w vm.max_map_count=262144
	sudo sysctl -w vm.swappiness=1
	sudo sysctl -w net.ipv4.tcp_retries2=5
	```
	- Agregar la siguientes configuraciones a nivel de SO (Sistema Operativo):
	```sh
	cat >> /etc/security/limits.conf  << EOF
	root hard nofile 65535
	root soft nofile 65535
	root soft nproc unlimited
	root hard nproc unlimited
	EOF
	```
	```sh
	echo "vm.max_map_count=262144" >> /etc/sysctl.conf
	echo "vm.swappiness=1" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_retries2=5" >> /etc/sysctl.conf
	```
	- Copiar los siguientes archivos:
	```sh
	\cp -av ansible/docker/daemon.json /etc/docker/daemon.json
	\cp -av ansible/docker/docker.service.d/override.conf /etc/systemd/system/docker.service.d/
	```
	- Reiniciar el servicio de docker:
	```sh
	sudo systemctl daemon-reload && sudo systemctl restart docker
	```

- Instalación de **Docker Composer**
	- Descargar y desempaquetar paquete en la siguiente ruta **`/usr/local/bin/docker-compose`**:
	```sh
	sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	```
	- Agregar permisos de ejecución y crear un enlace simbólico a **`/usr/bin/docker-compose`**:
	```sh
	sudo chmod +x /usr/local/bin/docker-compose
	sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
	```
	- Validar que el archivo **`production.yml`** es reconocido de manera correcta:
	```sh
	docker-compose -f docker-compose/test/test.yml config
	```

