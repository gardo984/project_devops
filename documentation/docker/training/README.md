# Docker Training

Glossary:
- [Docker Run](#docker-run)
- [Docker PS](#docker-ps)
- [Docker Inspect](#docker-inspect)
- [Docker Logs](#docker-logs)
- [Docker Stop](#docker-stop)
- [Docker Rm](#docker-rm)
- [Docker Images](#docker-images)
- [Docker Build](#docker-build)
- [Docker Compose](#docker-compose)
	- [Use](#use)
	- [Example](#example)
- [Volumes](#Volumes)
- [Networks](#Networks)
- [Docker Registry](#docker-registry)
	- [Installation](#installation)
	- [Push Images](#push-images)
- [Docker Swarm](#docker-swarm)
	- [Node Operations](#node-operations)
	- [Service Operations](#service-operations)
- [Enable Remote Access](#enable-remote-access)
	- [Ssh access](#ssh-access)
	- [Https access](#https-access)
	- [Docker Context](#docker-context)
- [References](#references)


## Docker Run

- To run a container:
	- In the foreground:
	```sh
	docker run nginx
	```
	- In the background
	```sh
	docker run -d nginx
	```
- To run a command in the created container:
```sh
docker run nginx cat /etc/**release**
```
- To run a container and attach to its terminal:
```sh
docker run -it nginx bash
```
- To run a container and publish its ports, the format is `Host Port` follow by `Container Port`:
	- By default `nginx` expose port `80`:
	```sh
	docker run -d -p 8001:80 nginx
	```
	- Once the container is `running`, validate the port nat:
	```sh
	$ curl -I http://localhost:8001
	HTTP/1.1 200 OK
	Server: nginx/1.21.6
	Date: Sun, 29 May 2022 23:56:44 GMT
	Content-Type: text/html
	Content-Length: 615
	Last-Modified: Tue, 25 Jan 2022 15:26:06 GMT
	Connection: keep-alive
	ETag: "61f0168e-267"
	Accept-Ranges: bytes
	```
- Run a container setting a environment variable:
```sh
docker run -d -e MYSQL_ROOT_PASSWORD=<password> mysql
```

## Docker Inspect

- To inspect details of a container:
```sh
docker inspect <container-name>
```
- Inspect details of a container applying an output format:
```sh
docker inspect <container-name> --format '{{.Config.Env}}'
```

## Docker Logs

- To check logs of a specific container:
```sh
docker logs -f <container-name>
```

## Docker PS

- List `running` containers:
```sh
docker ps
```
- List active containers `paused` and `running`:
```sh
docker ps -a
```
- List containers by filters:
> #### Important!
> For more details about the available filtering fields:
> - https://docs.docker.com/engine/reference/commandline/ps/#filtering
```sh
docker ps -a -f names=container_name -f state=running -f ancestor=image_name
```
- List containers applying an ouput format:
> #### Important!
> For more details about the available filtering fields:
> - https://docs.docker.com/engine/reference/commandline/ps/#formatting
```sh
docker ps -a -f ancestor=image_name --format '{{.Names}}:{{.Image}}'
```

## Docker Stop

- Stop a specific container (by `ID` or `Names`):
```sh
docker stop <container-id>
```
- Stop all containers:
```sh
docker stop $(docker ps -qa | xargs)
```

## Docker Rm

- Remove a specific container (by `ID` or `Names`):
```sh
docker rm <container-id>
```
- Remove all containers:
```sh
docker rm $(docker ps -qa | xargs)
```

## Docker Images

- List images:
```sh
docker images
```
- List images applying filters:
> #### Important!
> To check the available fields and type of filters, follow the next link:
> - https://docs.docker.com/engine/reference/commandline/images/#filtering
```sh
docker images nginx
```
- List images applying an output format:
> #### Important!
> To check the available fields and type of filters, follow the next link:
> - https://docs.docker.com/engine/reference/commandline/images/#format-the-output
```sh
docker images --format '{{.Repository}}:{{.Tag}}:{{.ID}}'
```
- Remove an image:
```sh
docker image rm <image-name>
```
- Remove more than an image:
```sh
docker rm $(docker images -q |xargs )
```

## Docker Build

> #### Important!
> For more details about the **`Dockerfile`**, check the file located in **`dockerfiles/Dockerfile.flask`**.
> - To validate each command specified in the **`Dockerfile`**, it can be tested running a container in attached mode, like the following (in other images the command specified is **`/bin/sh`** such alpines images):
> ```sh
> docker run -it ubuntu bash
> ```
> - To run a container in attached mode and mounted with your app directory `$PWD`(location of your absolute path):
> ```sh
> docker run -it -v $PWD:/opt/app-dir ubuntu bash
> ```

- To build an image, execute the following command with the code content of the directory **`source-codes/flask-app`**:
```sh
docker build -t flask-app-test:latest -f dockerfiles/Dockerfile.flask source-codes/flask-app/
```
- Validate the image was built:
```sh
docker images
```
- To run a container with the previous image built:
```sh
docker run -d -p 5000:5000 flask-app-test:latest
```
- Validate port was opened:
```sh
netstat -lntp |grep -i 5000
```
- Validate port is working as expected:
```sh
$ curl -I http://localhost:5000/welcome
HTTP/1.1 200 OK
Server: Werkzeug/2.1.2 Python/3.10.4
Date: Mon, 30 May 2022 01:12:15 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 44
Connection: close

<p>Welcome to a flask containerized app!</p>
```

## Docker Compose

### Use

- To check the services:
	- To check its services names:
	```sh
	docker-compose -f docker-compose.yml ps --services
	```
	- To check them in a common way:
	```sh
	docker-compose -f docker-compose.yml ps
	```
- To start the services:
	- Attach mode:
	```sh
	docker-compose -f docker-compose.yml up
	```
	- Dettach mode:
	```sh
	docker-compose -f docker-compose.yml up -d
	```
	- Perform a Build previous to start containers:
	```sh
	docker-compose -f docker-compose.yml up -d --build
	```
	- To start a specific service (for the example the target service is `backend`):
	```sh
	docker-compose -f docker-compose.yml up -d --build backend
	```
- Check logs, to cut the logging view  (control + c):
```sh
docker-compose -f docker-compose.yml logs -f
```
- Stop services:
	- Stop all services:
	```sh
	docker-compose -f docker-compose.yml stop
	```
	- Stop a specific service (for the example the target service is `backend`):
	```sh
	docker-compose -f docker-compose.yml stop backend
	```
- Destroy services:
```sh
docker-compose -f docker-compose.yml down
```
- Validate the consistency of a `docker-compose` file:
```sh
docker-compose -f docker-compose.yml config
```
- To build an image:
	- To build a specific service (for the example the target service is `backend`), the `--no-cache` flag is optional:
	```sh
	docker-compose -f docker-compose.yml build --no-cache backend
	```
	- To build all services involved:
	```sh
	docker-compose -f docker-compose.yml build build
	```
- To execute a command on a container:
	- Specify any command (for the example the command is `date`):
	```sh
	docker-compose -f docker-compose.yml exec backend date
	```
	- To attach to the running container:
	```sh
	docker-compose -f docker-compose.yml exec backend bash
	```
- To run a new instance in attached mode:
	- Common way:
	```sh
	docker-compose -f docker-compose.yml run backend bash
	```
	- To override the `entrypoint` property:
	```sh
	docker-compose -f docker-compose.yml run --entrypoint /bin/bash backend bash
	```

- To push an image:
```sh
docker-compose -f docker-compose.yml push
```

### Example

Go to the directory `./documentation/docker/training` and perform the following commands:
- Confirm the consistency of the `docker-compose.yaml` file:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml config
```
- Build an image:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml build --no-cache backend
```
- Start the container (in detached mode):
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml up -d backend
```
- Check Status:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml ps
```
- Check logs:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml logs -f backend
```
- Perform requests meanwhile you are checking the logs:
```sh
curl -I http://localhost:5000
```
- Perform a command inside the container:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml exec backend date
```
- Stop the services:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml stop backend
```
- Destroy the services:
```sh
docker-compose -f docker-compose/docker-compose.flask.yaml down
```

## Volumes

- To run a container with a volume in detach mode:
```sh
docker run -d --name mysql-db --e MYSQL_ROOT_PASSWORD=123456db -v mysql-volume:/var/lib/mysql mysql
```
- To run a container with a bind mount in detach mode:
```sh
docker run -d --name mysql-db \
	--e MYSQL_ROOT_PASSWORD=123456db \
	--mount type=bind,source=/opt/mysql,target=/var/lib/mysql mysql
```
- Validate the volumes:
```sh
docker volume ls
```
- To check volumes of a container (`jq` is a command that can be installed by executing `apt-get install -y jq`):
```sh
docker inspect <container-id> --format "{{json .Mounts}}" |jq
```

## Networks

- To list all available networks:
```sh
docker network ls
```
- To create a new internal network:
```sh
docker network create --driver bridge --subnet 182.18.0.0/16 --gateway 182.18.0.1 <network-name>
```
- To specify a container to use a network:
```sh
docker run -d --network <network-name> nginx
```
- To check the network of a container (`jq` is a command that can be installed by executing `apt-get install -y jq`):
```sh
docker inspect <container-id> --format "{{json .NetworkSettings}}" | jq
```

## Docker Registry

### Installation

- Run the command:
```sh
docker run -d --name registry -p 5000:5000 --restart always registry:2
```
- To validate that is available:
	- Performing a request:
	```sh
	curl -X GET localhost:5000/v2/_catalog | jq
	```
	- Checking open ports (`netstat` can be installed by `apt-get install net-tools -y`):
	```sh
	netstat -lntp |grep -i 5000
	```

### Push Images

- Lets pull 2 images `nginx:latest` and `httpd:latest`:
```sh
docker pull httpd:latest
docker pull nginx:latest
```
- Now create two tags bound to those images:
```sh
docker image tag nginx:latest localhost:5000/nginx:latest
docker image tag httpd:latest localhost:5000/httpd:latest
```
- Now push the 2 images:
```sh
docker push localhost:5000/nginx:latest
docker push localhost:5000/httpd:latest
```
- Validate that images were pushed:
```sh
curl -X GET localhost:5000/v2/_catalog | jq
```

## Docker Swarm

### Node operations

- Start swarm cluster:
```sh
$ docker swarm init 
```
- Join nodes into swarm cluster:
	> ### Importante!
	> When the cluster has more than one **`manager`**, in order to keep the cluster working properly you will need a minimum of master nodes online, docker applies a process called  **`Quorum`**, therefore, if the cluster did not have the required masters, the cluster will stop.
	>
	> When the master nodes can not be recovered, is necessary to recreate the cluster:
	> ```sh
	> $ docker swarm init --force-new-cluster 
	> ```
	- Get a token to join a node of type `worker`:
	```sh
	$ docker swarm join-token worker
	```
	- Get a token to join a node of type `manager`:
	```sh
	$ docker swarm join-token manager
	```
	- The output of the command is a command that should be executed in the node (client host):
	```sh
	$ docker swarm join --token SWMTKN-1-2utrm3jsg8c9fvvbvmwz1boodyfzosen9d4hrbqrzrm53xrt90-7vi9otbxjiynwuqyex708dqm3 <master-node>:<port-number>
	```
- Remove a node from the cluster:
	- Remove a node executed from a manager node:
	```sh
	$ docker node rm <node-name>
	```
	- Leave a cluster (executed from the worker node):
	```sh
	$ docker swarm leave
	```
- List available nodes (allowed just in the master nodes):
```sh
$ docker node ls
```
- Promote a worker node to a manager:
```sh
$ docker node promote <node-name>
```
- Downgrade a node from manager to worker:
```sh
$ docker node demote <node-name>
```
- Configure a node not to store containers (it is usually performed for maintenance purposes):
```sh
$ docker node update --availability drain <node-name>
```
- Inspect a node:
```sh
$ docker node inspect <node-name> --pretty
```

### Service operations

- List services:
```sh
$ docker services ls
```
- Create services:
```sh
$ docker services create --name <svc-name> <image>
```
- Update services:
```sh
$ docker services update <svc-name> --replicas=3 --publish-add <node-port>:<container-port>
```
- List the available containers per service:
	- The common way:
	```sh
	$ docker services ps <service-name>
	```
	- Using filters (for instance taking into consideration the one that are running):
	```sh
	$ docker services ps <service-name> -f "desired-state=running"
	```

### Stacks

- To validate the consistency of a file:
```sh
$ docker-compose -f <docker-compose-file> config
```
- To deploy a stack of services, use the following example:
```sh
$ docker stack deploy <stack-name> -c swarm/docker-stack.yml
```
- To remove a stack:
```sh
$ docker stack rm <stack-name>
```
- To list all stacks:
```sh
$ docker stack ls
```
- To list all services per stack:
```sh
$ docker stack services <stack-name>
```
- To list all containers per stack:
```sh
$ docker stack ps <stack-name>
```
- To check logs per service:
```sh
$ docker service logs <service-name>
```

## Enable Remote Access

To enable remote access, modify the file **`/etc/docker/daemon.json`** and add the following line:
```sh
"hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2375"]
```

### Ssh Access

- Create an user and add it to the docker group (perform this operation in the docker host):
```sh
useradd <username>
passwd <username>
usermod -aG docker <username>
```
- In the client side:
```sh
export DOCKER_HOST=ssh://<username>@<docker-hostname>
docker info
```

### Https Access

- On the **`Docker Server`**:
	- Generate a `Private` and `Self signed Public` CA keys:
	```sh
	openssl genrsa -aes256 -out /tmp/ca-key.pem 4096
	openssl req -new -x509 -sha256 -days 365 -key /tmp/ca-key.pem -out /tmp/ca.pem
	```
	- Generate a `Private` and `Certificate Sign Request` for the server:
	```sh
	openssl genrsa -out /tmp/server-key.pem 4096
	openssl req -subj "/CN=<docker-hostname>" -sha256 -new -key /tmp/server-key.pem -out /tmp/server.csr
	```
	- Create a `Extfile`:
	```sh
	echo "subjectAltName = DNS:<docker-hostname>,IP:127.0.0.1" >> /tmp/extfile.cnf
	echo "extendedKeyUsage = serverAuth" >> /tmp/extfile.cnf
	```
	- Sign `csr`:
	```sh
	openssl x509 -req -days 365 -sha256 \
		-in /tmp/server.csr \
		-CA /tmp/ca.pem -CAkey /tmp/ca-key.pem -CAcreateserial \
		-extfile /tmp/extfile.cnf \
		-out /tmp/server-cert.pem 
	```
	- Copy the certs **`ca.pem`**, **`server-key.pem`** and **`server-cert.pem`** to **`/root/.docker/`**
	```sh
	cp -avR /tmp/{ca,server-key,server-cert}.pem /root/.docker/
	```
	- Add the following lines in the **`/etc/docker/daemon.json`** (remember that TLS port is **`2376`**):
	```sh
	"hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2376"],
	"tlsverify": true,
	"tlscacert": "/root/.docker/ca.pem",
	"tlscert": "/root/.docker/server-cert.pem",
	"tlskey": "/root/.docker/server-key.pem"
	```
	- Restart docker service:
	```sh
	systemctl stop docker
	sleep 4
	systemctl start docker
	```
	- Generate `Private`, `CSR` and `Signed Certificate` for a remote connection:
	```sh
	$ openssl genrsa -out /tmp/client-key.pem 4096
	$ openssl req -new -subj "/CN=client" -sha256 -key /tmp/client-key.pem -out /tmp/client.csr
	$ echo "extendedKeyUsage = serverAuth" >> /tmp/extfile-client.cnf
	$ openssl x509 -req -days 365 -sha256 -in /tmp/client.csr \
		-CA /tmp/ca.pem -CAkey /tmp/ca-key.pem -CAcreateserial \
		-extfile /tmp/extfile-client.cnf \
		-out /tmp/client-cert.pem
	```
	- Change the permissions of the files:
	```sh
	$ chmod 400 -v /tmp/ca-key.pem /tmp/server-key.pem /tmp/client-key.pem
	$ chmod 444 -v /tmp/ca.pem /tmp/server-cert.pem /tmp/client-cert.pem
	```

- On the **`Docker Cli`**:
	- Copy the keys **`ca.pem`**, **`client-cert.pem`** and **`client-key.pem`** to your workstation on the following path **`~/.docker/certs/`**:
	- Create a context to connect to the remote docker host:
	```sh
	$ docker context create <context-name> --docker "host=tcp://<docker-host>:2376,ca=<ca-path>,cert=<cert-path>,key=<key-path>"
	$ docker context use <context-name>
	$ docker info
	```

### Docker Context

- To list contexts:
```sh
docker context ls
docker context ls -q
docker context list
```
- To show current context:
```sh
docker context show
```
- To create a context:
```sh
docker context create <context-name> --docker "host=tcp://<docker-host>:<docker-port>"
```
- To update a context:
```sh
$ docker context update <context-name> --description "test description"
$ docker context update <context-name> \
		--docker "host=tcp//<docker-host>:<docker-port>,ca=<ca-path>,cert=<cert-path>,key=<key-path>,skip-tls-verify=false"
```
- To use a specific context:
```sh
docker context use <context-name>
```
- To inspect a specific context:
```sh
docker context inspect <context-name>
```

## References

- Docker Compose Information: https://docs.docker.com/compose/
- Docker Compose CLI: https://docs.docker.com/engine/reference/commandline/compose/
- Docker Registry: https://docs.docker.com/registry/deploying/
- Docker Registry insecure (tests): https://docs.docker.com/registry/insecure/
- Docker core information:
	- https://docs.docker.com/engine/
	- https://docs.docker.com/engine/api/
	- https://docs.docker.com/config/containers/runmetrics/#control-groups
- Docker contexts: https://docs.docker.com/engine/context/working-with-contexts/
- Docker use of TLS: https://docs.docker.com/engine/security/protect-access/#use-tls-https-to-protect-the-docker-daemon-socket