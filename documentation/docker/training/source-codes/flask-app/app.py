from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


@app.route("/welcome")
def welcome():
    return "<p>Welcome to a flask containerized app!</p>"
