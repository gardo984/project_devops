
# Ansible

## Requisitos Previos
Seguir con los pasos de <a href="../../../ansible/playbooks#requisitos-previos" target="_blank">Instalación de Ansible</a> y <a href="../../../ansible/playbooks#comandos-disponibles" target="_blank">Comandos disponibles</a>.

## Instalación y Desinstalación de Servicios

- Instalación:
```sh
ansible-playbook install_docker.yaml -i hosts
```
- Desinstalación:
```sh
ansible-playbook uninstall_docker.yaml -i hosts
```
- Un playbook puede contener una variedad de **`Tasks`**, que a su vez cada task puede tener uno o varios **`Tag`s**, si en algún momento se desea ejecutar sólo tasks asociados a especificos tags, se podría realizar de la siguiente manera:
  - Ejecutar la instalación de docker con su configuración por **default**:
  	```sh
	ansible-playbook install_docker.yaml -t docker-installation
	```
  - Realizar la instalación de **`Docker composer`**:
  	```sh
	ansible-playbook install_docker.yaml -t composer-installation
	```
  - Realizar las configuraciones para un entorno **`productivo`** a nivel de SO (Sistema Operativo):
  	```sh
	ansible-playbook install_docker.yaml -t docker-tuning
	```