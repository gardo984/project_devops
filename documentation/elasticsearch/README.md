# Instalación

El siguiente repositorio contiene el glosario por el servicio **Elasticsearch** :
- Instalación y configuración.
	- [Manera Automatizada](#manera-automatizada).
		- Requisitos Previos.
		- Despliegue.
- [Notas Importantes](#notas-importantes).

Tener en cuenta que el despliegue ha sido realizado para la distribución de Linux **`CentOS 7 (versiones superiores) y Rocky Linux`**.

## Manera Automatizada

### Requisitos Previos
- <a href="../ansible/playbooks#requisitos-previos" target="_blank">Instalación de Ansible</a>.
- <a href="../docker#manera-automatizada" target="_blank">Instalación de Docker</a>.
- Para mayores detalles de lo que es posible realizar con Ansible, favor de seguir el siguiente <a href="../ansible/playbooks#comandos-disponibles" target="_blank"><b>enlace<b></a>.

### Despliegue
- Despliegue atravéz de ansible:
	- Ubicarse en el directorio de **playbooks**:
	```sh
	git clone -v "https://gitlab.com/gardo984/project_devops.git"
	cd ./documentation/elasticsearch/ansible/playbooks
	```
	- Ejecución del playbook:
	```sh
	ansible-playbook deploy_elastic.yaml
	```

## Notas Importantes

- Por algún proposito se necesite acceder a las plantillas de **Docker** utilizadas, se encuentran en la siguiente ruta:
```
documentation/elasticsearch/ansible/playbooks/docker-compose/elasticsearch/
```
- Para ejecución de las plantillas de **docker compose**:
	- Para entorno desarrollo:
	```sh
	docker-compose -f development.yml  up -d
	```
	- Para entorno productivo:
	```sh
	docker-compose -f production.yml  up -d
	```
- Validación que los servicios se encuentren ejecutandose:
	- Para entorno desarrollo:
	```sh
	docker-compose -f development.yml  ps
	```
	- Para entorno productivo:
	```sh
	docker-compose -f production.yml  ps
	```
- Para la validación de servicios, si solicita ingresar usuario y clave, ingresar los valores que se encuentran en el archivo **`.env`**. 
  - Estado **`elasticsearch cluster`**:
  ```sh
  http://localhost:9200/_cat/nodes?v=true&pretty
  ```
  - Estado **`Kibana`**: 
  ```sh
  http://localhost:5601/
  ```