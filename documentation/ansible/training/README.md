# Ansible Training

The current directory is a project containing information to test / practice ansible and all the features it has to offer.

Most of the playbooks file, will be located in the `playbook_examples` directory.

Glossary:
- [Modules](#modules)
- [Task Properties](#task-properties)
- [References](#references)

## Modules

Some of the common modules to highlight:
- **command**: execute a command in the remote host.
- **lineinfile**: add a line in a file.
- **service**: interact with OS services.
- **shell**:  to perform a command that accepts pipes and other special chars.
- **yum**: to install packages on CentOS hosts.
- **debug**: to create variables or print variable's value.
- **script**: execute a script in the remote host (it is copied by ansible).

## Task Properties

Some important task properties to highlight:
- **when**: to add a condition.
- **failed_when**: condition to validate a failure.
- **loop**: to perform a iteration.
- **loop_extended**: enable additional features of the `loop` property.
- **vars**: add environment variables `globally` or per `task`.

## References

- Core modules: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html
- Debugging: https://docs.ansible.com/ansible/latest/user_guide/playbooks_debugger.html#redo-command
- Handling errors: https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html
- Ansible configurations: https://docs.ansible.com/ansible/latest/installation_guide/intro_configuration.html#configuration-file}
