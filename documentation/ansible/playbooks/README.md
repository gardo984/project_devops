
# Ansible

Glosario:
- [Requisitos Previos](#requisitos-previos)
- [Comandos Disponibles](#comandos-disponibles)
	- [Configuración](#configuración)
	- [Playbooks](#playbooks)
	- [Vault](#vault)
- [Ejecución remota de tasks](#ejecución-remota-de-tasks)

## Requisitos Previos
- Instalar python y herramientas desarrollo:
> ### Importante!
> Para `Mac OS` instalar `sshpass` de la siguiente manera:
> ```sh
> $ brew install esolitos/ipa/sshpass
> ```
```sh
yum install python3 python3-devel sshpass -y
```
- Instalar ansible atravéz de pip3
```sh
pip3 install --upgrade pip
pip3 install ansible docker docker-compose
ansible-galaxy collection install community.docker
```
- Validar versión instalada:
```sh
ansible --version
```
- **Opcional**: Una herramienta muy importante para el desarrollo de playbooks **`ansible-lint`**:
```sh
pip3 install ansible-lint
```

## Comandos Disponibles

### Configuración
- Identificar archivo de configuración:
```sh
ansible-config view -v
```
- Se podrá visualizar algunas opciones asignadas por defecto:
	- Archivo **inventory** que contendrá el listado de hosts agrupados por sección:
	```
	inventory= ./hosts
	```
	- Archivo que contendrá la clave de **ansible vault** (para encriptar archivos):
	```
	vault_password_file = ./.vault_pass
	```
	- Configuraciones adicionales pueden ser difinidas dentro del directorio **`group_vars`**, la configuración se aplica por **`grupo de inventario`**, como grupo por defecto es **`servers`** del archivo **`./hosts`**:
	```
	group_vars/servers/vars
	```

### Playbooks

>>>
###### Importante

Variables de entorno pueden ser definidas de manera interna (mismo playbook) o de manera externa como el caso de los archivos del directorio **`vars/`**
>>>

- Validar sintaxis del playbook:
```sh
ansible-playbook --syntax-check tests.yaml
```
- Listado de **`Tags`**:
```sh
ansible-playbook --list-tags tests.yaml
```
- Listado de **`Tasks`**:
```sh
ansible-playbook --list-tasks tests.yaml
```
- Listado de **`Hosts`**:
```sh
ansible-playbook --list-hosts tests.yaml
```
- Información adicional de uso:
```sh
ansible-playbook -h
```
- Ejecución del playbook:
```sh
ansible-playbook tests.yaml
```

### Vault

>>>
###### Importante

Sí el parametro **`vault_password_file`** ha sido especificado en el archivo de configuración de Ansible, los comandos siguientes no solicitarán clave, caso contrario sí será solicitado.
>>>

Consiste en un mecanismo de **`encriptar / desencriptar`** valores en un archivo plano, que puede contener valores como:
- claves de autenticación.
- hashes ó tokens de acceso.
- información sensible.

El directorio **`group_vars/servers/`** contiene un archivo **`vault`** el cual contiene información encriptada que es utilizada / invocada en el archivo **`vars`**. Adicionalmente se detallan algunas operaciones útiles:

- Visualizar contenido de archivo encriptado:
```sh
ansible-vault view group_vars/servers/vault
```
- Procedimiento para desencriptar un archivo:
	- Ejecutar comando:
	```sh
	ansible-vault decrypt group_vars/servers/vault
	-> Decryption successful
	```
	- Corroborar que contenido ha sido desencriptado:
	```sh
	cat group_vars/servers/vault
	```
- Procedimiento para encriptar un archivo:
	- Ejecutar comando:
	```sh
	ansible-vault encrypt group_vars/servers/vault
	-> Encryption successful
	```
	- Corroborar que contenido ha sido encriptado:
	```sh
	cat group_vars/servers/vault
	```
- Editar contenido de archivo encriptado:
```sh
ansible-vault edit group_vars/servers/vault
```
- Procedimiento para cambiar clave vault:
	- Generar una nueva clave aleatoria y asignar el nuevo archivo **`.vault_pass`**:
	```sh
	cat /proc/sys/kernel/random/uuid  |openssl base64 > .new_vault_pass
	ansible-vault rekey group_vars/servers/vault --new-vault-password-file .new_vault_pass 
	-> Rekey successful
	cat .new_vault_pass > .vault_pass 
	rm -rf .new_vault_pass
	```
	- Validar que la clave generada del archivo **`.vault_pass`** es la correcta:
	```sh
	ansible-vault view group_vars/servers/vault --ask-vault-password
	```

## Ejecución remota de tasks

>>>
###### Importante

Para la autenticación durante la ejecución de playbooks, se encuentran disponibles los siguientes parametros en el archivo **`group_vars/servers/vars`**:
- `ansible_user`.
- `ansible_password`.
- `ansible_ssh_private_key_file`.
- `ansible_become_user`.
- `ansible_become_password`.

Si alguno de los parametros previos mencionados son configurados, ya no será necesario especificar los parametros **`-k`** y **`-K`** durante la ejecución del **`playbook`**.
>>>

- Pongamos el escenario de realizar una instalación / mantenimiento en 2 equipos remotos: **`elastic-node1`** y **`elastic-node2`**, para referenciarnos ante los equipos por nombre, los agregaremos en el archivo **`/etc/hosts`**:
```sh
192.168.0.12    elastic-node2
192.168.0.11    elastic-node1
```
- En el archivo **`playbooks/hosts`** agregaremos los siguientes hosts:
```yaml
[servers]
elastic-node1
elastic-node2
```
- Ejecutaremos el siguiente playbook **`tests.yaml`** que nos listará los paquetes instalados de cada equipo:
  - Propiedad `hosts`: Representa el segmento de equipos del archivo **`playbooks/hosts`**.
  - Propiedad `become`: Dará a entender que se escalará privilegios para la ejecución del comando.
  - Propiedad `remote_user`: Usuario con el cuál será autenticado al conectarse al host destino.
	```yaml
	---
	- name: tests on remote host
	  gather_facts: No
	  hosts: servers
	  remote_user: account
	  become: yes

	  tasks:
	    - name: get packages list
	      ansible.builtin.command: rpm -qa
	```
- Para la ejecución de un playbook, proceder con el siguiente comando:
	- Con usuario **root**
	```sh
	ansible-playbook -i hosts tests.yaml
	```
	- Con escalamiento de privilegios **`sudo`**, **`-k`** (solicitará clave de autenticación ssh), **`-K`** (solicitará clave para escalar privilegios):
	```sh
	ansible-playbook -k -K -v -i hosts tests.yaml
	```

- Posible resultado sería algo asi:
```
SSH password: 
BECOME password[defaults to SSH password]: 

PLAY [tests on remote host] ********************************************************************************************************************************************

TASK [get packages list] ***********************************************************************************************************************************************
changed: [elastic-node1] => {"ansible_facts": {"discovered_interpreter_python": "/usr/bin/python"}, "changed": true, "cmd": ["rpm", "-qa"], "delta": "0:00:01.332621", "end": "2021-08-23 14:38:26.659609", "rc": 0, "start": "2021-08-23 14:38:25.326988", "stderr": "", "stderr_lines": [], "stdout": "plymouth-core-libs-0.8.9-0.34.20140113.el7.centos.x86_64\nsetup-2.8.71-11.el7.noarch\nvirt-what-1.18-4.el7.x86_64\nncurses-base-5.9-14.20130511.el7_4.noarch\nrpm-python-4.11
....
....
....
....
PLAY RECAP *************************************************************************************************************************************************************
elastic-node1 : ok=1    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0 
```