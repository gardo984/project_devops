# Microk8s

El presente documento abarcará la configuración del servicio una vez instalado.

Glosario:
- Comandos importantes
- Habilitación de Pluggins
- Habilitación de Admin Dashboard

## Habilitación de Pluggins
- Habilitar los siguientes plugins
```sh
microk8s enable dns dashboard storage
```
## Habilitación Admin Dashboard
- Habilitar Admin dashboard plugins:
```sh
microk8s enable dashboard
```
- Listar los secretos / servicios disponibles:
```sh
kubectl -n kube-system get secret
```
- Obtener el token generado durante la instalación:
```sh
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret |grep -i default | cut -d " " -f1)
```
- Una vez después de haber obtenido el token, publicar el admin dashboard:
```sh
kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443 --address 0.0.0.0
```
- Después desde su máquina local podrá acceder al admin dashboard atravéz de la url `https://<cluster-ip>:10443` ó validarlo atravéz de líneas de comandos:
```sh
curl -I --insecure https://<cluster-ip>:10443
HTTP/2 200 
accept-ranges: bytes
cache-control: no-cache, no-store, must-revalidate
content-type: text/html; charset=utf-8
last-modified: Thu, 18 Feb 2021 14:45:44 GMT
content-length: 1250
date: Sat, 16 Oct 2021 17:28:34 GMT
```