

## Copy manually a virtual machine vmware on Linux
- Copy the whole directory to a new one
```sh
\cp -avR old_name new_name
cd new_name
```
- Rename all files : 
```sh
rename  -v -e 's/old_name/new_name/' *.*
```
- Replace the content of the files about old_name with new_name : 
```sh
sed -i -E 's/old_name/new_name/g' *.vmx*
```
- Replace the content of files lower than 2k and extension ".vmdk*"
```sh
sed -i -E 's/old_name/new_name/g' $(find . -size -2k -iname "*.vmdk")
```
