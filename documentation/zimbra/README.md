
# Zimbra Notes

In this article you are gonna find all kinda content related to zimbra.

Glossary:
- [Installation](#installation):
	- [Centos 7](#centos_7)

## Installation

Some platforms where zimbra can be installed.

### Centos 7

> #### Important
> If you are setting up a Centos 7 OS, will be important to replace some content on the repos files:
> ```sh
> sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/CentOS-*.repo
> sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/CentOS-*.repo
> sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/CentOS-*.repo
> ```
> Then:
> ```sh
> yum update -y
> ```