
# Logging

The following directory will manage all information about:

- Data collector
- Data processor
- Data Shipper

Using:

- Fluentd
- Fluent Bit

Glossary:

- [Requirements](#requirements)
- [Installation](#installation)
	- [Fluentd](#fluentd)
	- [FluentBit](#fluentbit)
- [Integrations](#integrations)
	- [Nginx](#nginx)
	- [Elasticsearch](#elasticsearch)
- [References](#references)

## Requirements

- Configure that ntp service is enabled and synchronized:
	- Check configuration:
	```sh
	timedatectl
	```
	- To enable synchronization:
	```sh
	timedatectl set-ntp on
	```

- Increase the file descriptor limits:
	- Check configuration:
	```sh
	ulimit -Sn
	ulimit -Hn
	```
	- To increase the values, go to the file `/etc/security/limits.conf` and the following at end of the file:
	```conf
	root soft nofile 65536
	root hard nofile 65536
	```	
	- Finally, proceed to restart the host:
	```sh
	reboot
	init 6
	```	

- Tuning networking settings:
	- To system configurations:
	```sh
	sysctl -a | egrep -i "ip_local_port_range|tcp_tw_reuse"
	cat /proc/sys/net/ipv4/tcp_tw_reuse
	cat /proc/sys/net/ipv4/ip_local_port_range
	```
	- To update system values permanently, add the following values at the end of the file `/etc/sysctl.conf`:
	```conf
	net.ipv4.tcp_tw_reuse = 1
	net.ipv4.ip_local_port_range = 10240 65535
	```
	- Finally, reload all the system configuration:
	```sh
	sysctl -f
	```


## Installation

### Fluentd

#### Linux

- Install ruby:
```sh
sudo apt-get install -y ruby-full ruby-dev
```
- Check ruby version:
```sh
ruby -v
```
- Install the additional packages:
```sh
sudo apt-get install -y libssl-dev libreadline-dev zlib1g-dev make gcc
```
- Install ruby packages:
```sh
sudo gem install bundle
ls /var/lib/gems/3.0.0/gems/
```
- Finally:
```sh
sudo gem install fluentd
fluentd --help
fluentd --version
fluentd --setup ./fluent
```
- To run fluentd with a config file:
```sh
fluentd -c config.conf -vv
```
- To refresh a configuration file:
```sh
pkill -SIGUSR2 fluentd
```
- To flush the temporary cache:
```sh
pkill -SIGUSR1 fluentd
```

#### Docker

- Create a file in `/root/fluent/docker.conf` with the following content:
```xml
<source>
	@type http
	port 24220
	bind 0.0.0.0
</source>

<source>
	@type forward
	port 24224
	bind 0.0.0.0
</source>

<match **>
	@type stdout
</match>
```
- Proceed to run the following docker command:
```sh
docker run -d -p 24220:24220 -p 24224:24224 \
	-v ${HOME}/fluent/docker.conf:/fluentd/etc/docker.conf \
	-e FLUENTD_CONF=docker.conf \
	--name fluentd fluent/fluentd:edge-debian
```
- To validate that fluent container is running correctly:
```sh
docker logs -f fluentd
```
- Send a payload to the `http` port `24220`:
```
curl -d 'json={"msg": "hello world"}' http://localhost:24220/test
```
- Configure a container to send logs to the logger instance on the port `24224`:
```
docker run -d -P --log-driver=fluentd \
			--log-opt fluentd-address=localhost:24224 \
			--name http nginx:latest
```
- To check logs:
```sh
docker container logs fluentd |& sed -n -e '/nginx/,/.*/ p'
```
- To stop containers:
```sh
docker stop http-nginx fluentd
docker rm -f http-nginx fluentd
```

#### Kubernetes

- Create a file with the following content:
```xml
<source>
	@type http
	port 24220
	bind 0.0.0.0
</source>


<source>
	@type forward
	port 24224
	bind 0.0.0.0
</source>

<match **>
	@type stdout
</match>
```
- Create a configmap:
```sh
k create cm cm-fluentd --from-file=/root/fluent/fluentd-kube.conf
```
- Create a deploy with the following content:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: deploy-fluentd
    stack: fluentd
  name: deploy-fluentd
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deploy-fluentd
      stack: fluentd
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: deploy-fluentd
        stack: fluentd
    spec:
      volumes:
      - name: vol-fluentd-conf
        configMap:
         name: cm-fluentd

      containers:
      - image: fluent/fluentd:edge-debian
        name: fluentd
        ports:
        - containerPort: 24220
        volumeMounts:
        - name: vol-fluentd-conf
          mountPath: /fluentd/etc/
          readOnly: true
        env:
        - name: FLUENTD_CONF
          value: fluentd-kube.conf
        resources: {}
status: {}
```
- Create a service with the following content:
```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: svc-fluentd
    stack: fluentd
  name: svc-fluentd
spec:
  ports:
  - name: 24220-24220
    port: 24220
    protocol: TCP
    targetPort: 24220
  selector:
    stack: fluentd
  type: ClusterIP
status:
  loadBalancer: {}
```
- Create the objects:
```sh
k create -f deploy-fluentd.yaml
k create -f svc-fluentd.yaml
```
- To check logs:
```sh
k logs -f deployment/deploy-fluentd
```
- Test the request by service ip:
```sh
curl -d 'json={"msg": "hello world 2"}' http://<svc-ip>:24220/test
```
- Test the request from a pod:
```sh
k delete pod test-client && kubectl run test-client --image centos:8 -it
```
```sh
curl -d 'json={"msg":"Hello world"}' http://svc-fluentd.default.svc.cluster.local:24220/test
curl -d 'json={"msg":"Hello world"}' http://<service-ip>:24220/test
```


### FluentBit

#### Linux

- Create the following directory `./fluent-bit`:
```sh
mkdir -p ./fluent-bit
```
- Download and install the following package:
```sh
sudo curl -s https://raw.githubusercontent.com/fluent/fluent-bit/master/install.sh | sh
```
- Look for the fluent-bit executable:
```sh
sudo find / -type f -name "*fluent-bit*"
```
- Create a symbolic link:
```sh
sudo ln -s /opt/fluent-bit/bin/fluent-bit /usr/bin/fluent-bit
```
- Check the version:
```sh
fluent-bit --version
```
- Fluent-bit can be executed interactively for testing purposes:
```sh
fluent-bit --help | egrep -i "\-i|\-o|\-F"
```

#### Docker

- Create a file in `/root/fluent-bit/docker.conf` with the following content:
```yaml
[INPUT]
	name http
	host 0.0.0.0
	port 24220

[INPUT]
	Name forward
	Listen 0.0.0.0
	Port 24224
	Buffer_Chunk_Size 1M
	Buffer_Max_Size 6M

[OUTPUT]
	name stdout
	match *
```
- Run the following command to create a container with fluent-bit:
```sh
docker run -d -p 24220:24220 -p 24224:24224 \
		-v ${HOME}/fluent-bit/docker.conf:/fluent-bit/etc/fluent-bit.conf \
		--name fluent-bit fluent/fluent-bit:latest
```
- To check if container is running correctly:
```sh
docker ps
docker logs -f fluent-bit
```
- To send a payload to the port `24220` (http):
```sh
curl -H 'Content-Type: application/json' -d '[{"msg": "hello world"}]' http://localhost:24220/test
```
- Run a container and configure it to send its logs to the fluent-bit logging layer `24224`:
```sh
docker run -d -P --log-driver=fluentd \
		--log-opt fluentd-address=localhost:24224 \
		--name http-nginx nginx:latest
```
- To check logs:
```sh
docker container logs fluent-bit |& sed -n -e '/nginx/,/.*/ p'
```
- To stop containers:
```sh
docker stop http-nginx fluent-bit
docker rm -f http-nginx fluent-bit
```

#### Kubernetes

- Create the following k8s resources:
	- Config map content `cm-fluent-bit.yaml`:
	```yaml
	apiVersion: v1
	data:
	  fluent-bit.conf: "\n[INPUT]\n\tname http\n\thost 0.0.0.0\n\tport 24220\n\n[INPUT]\n\tName
	    forward\n\tListen 0.0.0.0\n\tPort 24224\n\tBuffer_Chunk_Size 1M\n\tBuffer_Max_Size
	    6M\n\n[OUTPUT]\n\tname stdout\n\tmatch *\n"
	kind: ConfigMap
	metadata:
	  creationTimestamp: null
	  name: cm-fluent-bit
	```
	- Deploy content `deploy-fluent-bit.yaml`:
	```yaml
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  creationTimestamp: null
	  labels:
	    app: deploy-fluent-bit
	    stack: fluent-bit
	  name: deploy-fluent-bit
	spec:
	  replicas: 1
	  selector:
	    matchLabels:
	      app: deploy-fluent-bit
	      stack: fluent-bit
	  strategy: {}
	  template:
	    metadata:
	      creationTimestamp: null
	      labels:
	        app: deploy-fluent-bit
	        stack: fluent-bit
	    spec:
	      volumes:
	      - name: vol-fluent-bit-conf
	        configMap:
	         name: cm-fluent-bit

	      containers:
	      - image: fluent/fluent-bit:latest
	        name: fluent-bit
	        ports:
	        - containerPort: 24220
	        volumeMounts:
	        - name: vol-fluent-bit-conf
	          mountPath: /fluent-bit/etc/
	          readOnly: true
	        resources: {}
	status: {}
	```
	- Service content `svc-fluent-bit.yaml`:
	```yaml
	apiVersion: v1
	kind: Service
	metadata:
	  creationTimestamp: null
	  labels:
	    app: svc-fluent-bit
	    stack: fluent-bit
	  name: svc-fluent-bit
	spec:
	  ports:
	  - name: 24220-24220
	    port: 24220
	    protocol: TCP
	    targetPort: 24220
	  selector:
	    stack: fluent-bit
	  type: ClusterIP
	status:
	  loadBalancer: {}
	```
	- Create the objects:
	```sh
	k create -f cm-fluent-bit.yaml
	k create -f deploy-fluent-bit.yaml
	k create -f svc-fluent-bit.yaml
	```

- Test the created resources:
```sh
k exec -it pod/test-client -- bash
curl -H 'Content-Type' -d '[{"msg": "hello world"}]' http://svc-fluent-bit.default.svc.cluster.local:24220/test
curl -H 'Content-Type' -d '[{"msg": "hello world"}]' http://<service-ip>:24220/test
```
- Check logs:
```sh
k logs -f deployment/deploy-fluent-bit
```

## Integrations

### Nginx

- To deploy nginx through docker:
```sh
docker run -d -p 80:80 -v "/tmp/logs/nginx:/var/log/nginx" --name nginx nginx:latest
```
- To get the container ip:
```sh
docker inspect nginx -f '{{range .NetworkSettings.Networks}} {{.IPAddress}} {{end}}'
```
- To check logs:
```sh
tail -f /tmp/logs/nginx/{access,error}.log
```
- Hit the nginx port:
```sh
curl -I http://<nginx-ip>
```

### Elasticsearch

- To deploy elasticsearch through docker:
```sh
docker run -d --name elasticsearch  -p 9200:9200 -e "discovery.type=single-node" \
	docker.elastic.co/elasticsearch/elasticsearch:8.14.3
```
- To get the container ip:
```sh
elastic_host=$(docker inspect elasticsearch -f '{{range .NetworkSettings.Networks}} {{.IPAddress}} {{end}}')
```
- Additional configs:
	- Reset admin password:
	```sh
	docker container exec -it elasticsearch /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic
	```
	- Save the password in a file `es_pwd.txt` and loads into a variable:
	```sh
	elastic_pwd=$(cat es_pwd.txt)
	```
	- Copy the certificate for authentication:
	```sh
	docker container cp elasticsearch:/usr/share/elasticsearch/config/certs/http_ca.crt es_http_ca.crt
	```
- Perform a test with curl hitting the elasticsearch port:
```sh
curl -u elastic:${elastic_pwd} --cacert ${PWD}/es_http_ca.crt https://172.17.0.3:9200
```
```json
{
  "name" : "ae4add292ca2",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "KTIPgOW2SRe8u3iSx7shtw",
  "version" : {
    "number" : "8.14.3",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "d55f984299e0e88dee72ebd8255f7ff130859ad0",
    "build_date" : "2024-07-07T22:04:49.882652950Z",
    "build_snapshot" : false,
    "lucene_version" : "9.10.0",
    "minimum_wire_compatibility_version" : "7.17.0",
    "minimum_index_compatibility_version" : "7.0.0"
  },
  "tagline" : "You Know, for Search"
}
```
- Look for elastic plugins:
```sh
fluent-gem list --remote fluent-plugin |egrep -i "elasticsearch|nginx-error"
```
- Install fluent plugin:
	- Recommended packages:
	```sh
	fluent-gem install \
		'fluent-plugin-nginx-error-multiline:~>0.2.0' \
		'elasticsearch:~>7.17.1' \
		'elasticsearch-transport:~>7.17.1' \
		'fluent-plugin-elasticsearch:~>5.2.3'
	```
	- Normal packages:
	```sh
	fluent-gem install fluent-plugin-nginx-error-multiline fluent-plugin-elasticsearch
	```
- Verify that packages were installed:
```sh
fluent-gem list --local |egrep -i "elastic|nginx"
```
- To verify available indexes:
```sh
curl  -u elastic:$elastic_pwd --cacert $PWD/es_http_ca.crt -k https://localhost:9200/_cat/indices?v
```
- To query a specific index:
```sh
curl  -u elastic:$elastic_pwd --cacert $PWD/es_http_ca.crt -k https://localhost:9200/fluentd/_search?pretty
```

#### Elasticsearch Fluent Match

- For more information about the [pluging parameters](https://github.com/uken/fluent-plugin-elasticsearch):
```xml
<match nginx.**>
	@type elasticsearch
	@log_level warn
	#logstash_format true
	#logstash_prefix test
	#logstash_dateformat %Y-%m-%d
	host 172.17.0.3
	port 9200
	user elastic
	password "#{ENV['elastic_pwd']}"
	scheme https
	ssl_verify false
	ca_file /root/fluent/labs/lab3/es_http_ca.crt
	#verify_es_version_at_startup false
	#default_elasticsearch_version 7
</match>
```

#### Elasticsearch Fluent-bit Output

- For more information about the [pluging parameters](https://docs.fluentbit.io/manual/pipeline/outputs/elasticsearch):
```ini
[OUTPUT]
	name es
	match *
	host 172.17.0.3
	port 9200
	index webservers
	http_user elastic
	http_passwd ${elastic_pwd}
	tls On
	tls.verify Off
	tls.ca_file /root/fluent-bit/labs/lab3/es_http_ca.crt
	Suppress_Type_Name On
```


## References

- Docker Log-Driver: https://docs.docker.com/config/containers/logging/fluentd/
- Coredns Bind permission denied: https://github.com/kubernetes/minikube/issues/17804#issuecomment-2216652046
- Elasticsearch fluentd: https://github.com/uken/fluent-plugin-elasticsearch
- Elasticsearch fluent-bit : https://docs.fluentbit.io/manual/pipeline/outputs/elasticsearch
