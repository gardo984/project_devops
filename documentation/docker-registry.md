

## Docker registry deployment

#### Previous steps to deployment
- create the cert and key files :
    ```bash
    $ openssl req -newkey rsa:4096 -nodes \
        -sha256 -keyout /etc/httpd/certs/domain.key \
        -x509 -days 365 -out /etc/httpd/certs/domain.crt \
        -subj '/C=PE/L=City/O=Company Name/OU=/CN=domain.com/emailAddress=contact@email.com'
    ```
- validates the generated files (private key and self-signed cert), [reference](https://www.sslshopper.com/article-most-common-openssl-commands.html) :
    ```bash
    $ openssl rsa -in /etc/httpd/certs/domain.key -check
    $ openssl x509 -in /etc/httpd/certs/domain.crt -text -noout
    ```
- enable registry authentication:
    ```bash
    $ docker run --entrypoint htpasswd httpd:2.4 \
        -Bbn username password > /etc/httpd/auths/htpasswd
    ```
- once the certs and authentication account have been generated, proceed with the deployment :
	- [Deployment through docker run](#deployment-with-docker-run)
	- [Deployment through docker-compose](#deployment-with-docker-compose)

#### Deployment with docker run
- deploy the registry container
    ```bash
    $ docker run -d -it --restart=always --name registry \
	-v /opt/docker-registry:/var/lib/registry \
	-v /etc/httpd/auths:/auths \
	-e REGISTRY_AUTH=htpasswd \
	-e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
	-e REGISTRY_AUTH_HTPASSWD_PATH=/auths/htpasswd \
	-v /etc/httpd/certs:/certs \
	-e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  	-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  	-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  	-p 443:443 registry:2
    ``` 
- validates the exposed port is enabled :
    ```bash
    $ sudo netstat  -ltnp |grep -i 443
    ```
- login in the docker registry, the prompt will ask for credentials :
    ```bash
    $ docker login domain.com
    ```     
- validate the pull and push processes from another client :
    ```bash
     $ docker pull domain.com/image1
     $ docker push domain.com/image2
    ```
- to query the available images in your repo query the following url :
    ```bash
    $ curl --insecure -u username:passwd https://domain.com/v2/_catalog
    ```     
#### Deployment with docker-compose
- create a file "**docker-compose.yml**" with the following content :
    ```yaml
    version: "3.9"
    services:
     registry:
      restart: always
      image: registry:2
      ports:
        - 443:443
      environment:
        REGISTRY_HTTP_TLS_CERTIFICATE: /certs/domain.crt
        REGISTRY_HTTP_TLS_KEY: /certs/domain.key
        REGISTRY_AUTH: htpasswd
        REGISTRY_AUTH_HTPASSWD_PATH: /auths/htpasswd
        REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
        REGISTRY_HTTP_ADDR: 0.0.0.0:443
      volumes:
        - /opt/docker-registry:/var/lib/registry
        - /etc/httpd/certs:/certs
        - /etc/httpd/auths:/auths
    ```     
- deploy the services:
    ```bash
    $ docker-compose -f docker-compose.yml up -d
    ```
- validates the state:
    ```bash
    $ docker-compose -f docker-compose.yml ps
    ```
    