
# Certificate Management

The current document will contain information about how to generate, sign and manage certificate files with the tool **`openssl`**.

> #### Important:
> - `openssl genrsa`: generate a RSA private keys.
> - `openssl req`: create and processes certificate requests.
> - `openssl x509`: multipurpose certificate utility for displaying cert info, convert certs to various forms and sign certificate requests.
> - `openssl s_client`: implements a SSL/TLS client which connects to a remote host using SSL/TLS.
> - `openssl rsa`: process RSA keys.

Glossary:
- [Private Keys](#private-keys)
- [CA Private and Public Keys](#ca-private-and-public-keys)
- [CSR generation](#csr-generation)
- [Sign CSR](#sign-csr)
- [Query Remote Certs](#query-remote-certs)
- [Certs Consistency](#certs-consistency)
- [References](#references)

## Private Keys

- Generate a private key without encryption:
```sh
openssl genrsa -out /tmp/output-key.pem 4096
```
- Generate a private key with encryption, when the encryption flag is used a **pass phrase** will be prompted:
```sh
openssl genrsa -aes256 -out /tmp/output-key.pem 4096
```
- Once generated, take a look of the file and its permission:
	- Input commands:
	```sh
	$ ls -ltrd /tmp/output-key.pem
	$ cat /tmp/output-key.pem  | head -n4
	```
	- Output:
	```sh
	-----BEGIN RSA PRIVATE KEY-----
	Proc-Type: 4,ENCRYPTED
	DEK-Info: AES-256-CBC,F7A665A92423E283A6BE14B729EA370C
	...
	...
	```
- To decode and check its content:
```sh
openssl rsa -in output-key.pem -text -noout
```

## CA Private and Public Keys

- Create an encrypted private key:
```sh
openssl genrsa -aes256 -out /tmp/ca-key.pem 4096
```
- Create a self-signed certificate as CA:
```sh
openssl req -new -x509 -days 365 -key /tmp/ca-key.pem -sha256 -out /tmp/ca.pem
```
- To decode and check the content:
	- To display the content in **`text`** format:
	```sh
	openssl x509 -in /tmp/ca.pem  -text -noout
	```
	- To display the **`subject`**:
	```sh
	openssl x509 -in /tmp/ca.pem  -noout -subject
	```
	- To display the **`public key`**:
	```sh
	openssl x509 -in /tmp/ca.pem  -noout -pubkey
	```

## CSR generation

- Create a private key without encryption:
```sh
openssl genrsa -out /tmp/server-key.pem 4096
```
- Create a CSR (replace **`domain.com`** by your own domain):
```sh
openssl req -subj "/CN=domain.com" -sha256 -new -key /tmp/server-key.pem -out /tmp/server.csr
```
- To decode and verify the **`CSR`** file generated:
	- To display the content in `text` format:
	```sh
	openssl req -in /tmp/server.csr -text -noout
	```
	- To display the **`subject`**:
	```sh
	openssl req -in /tmp/server.csr -noout -subject
	```
	- To display the **`pubkey`**:
	```sh
	openssl req -in /tmp/server.csr -noout -pubkey
	```
	- To verify the **`CSR`**:
	```sh
	openssl req -in /tmp/server.csr -noout -verify
	```

- Add additional config to `extfile`:
```sh
echo "subjectAltName = DNS:domain.com,IP:0.0.0.0,IP:127.0.0.1" >> extfile.cnf
echo "extendedKeyUsage = serverAuth" >> extfile.cnf
```

## Sign CSR

- To sign a CSR:
```sh
openssl x509 -req -days 365 -sha256 \
	-in server.csr \
	-CA ca.pem -CAkey ca-key.pem \
	-CAcreateserial  -extfile extfile.cnf \
	-out server-cert.pem
```

## Query Remote Certs

- To query a remote cert from a web page:
```sh
echo | openssl s_client -servername <server-name> -connect <domain>:443 2> /dev/null | openssl x509 -text -noout
```

## Cert Consistency

- Validate that **`Private Key`**, **`Certificate Sign Request`** and **`Certificate`** match (the generated hashes should be similar):
```sh
openssl pkey -in /tmp/key.pem -pubout | openssl sha256
openssl req -in /tmp/cert.csr -noout -pubkey | openssl sha256
openssl x509 -in /tmp/cert.pem -noout -pubkey | openssl sha256
```

## References

- Docker use of TLS: https://docs.docker.com/engine/security/protect-access/#use-tls-https-to-protect-the-docker-daemon-socket
- Query cert of a web page: https://www.howtouselinux.com/post/openssl-command-to-generate-view-check-certificate