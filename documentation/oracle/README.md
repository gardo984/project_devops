
# Oracle

Glossary:
- [Installation](#installation)
     - [Linux Platforms](#linux-platforms)
     - [Docker](#docker)
- [Configuration](#configuration)
- [Usage](#usage)
- [References](#references)

## Installation

### Linux Platforms

- download oracle rpm, take into consideration "21c XE linux OL8":
https://www.oracle.com/database/technologies/xe-downloads.html

- installation steps:
https://docs.oracle.com/en/database/oracle/oracle-database/21/xeinl/installing-oracle-database-free.html#GUID-46EA860A-AAC4-453F-8EEE-42CC55A4FAD5

- download dependency from:
```sh
wget -O compat-openssl10-1.0.2o-4.el8.x86_64.rpm https://rpmfind.net/linux/centos/8-stream/AppStream/x86_64/os/Packages/compat-openssl10-1.0.2o-4.el8.x86_64.rpm
yum localinstall compat-openssl10-1.0.2o-4.el8.x86_64.rpm
```

### Docker

- Pull the following image [gvenzl/oracle-xe](https://hub.docker.com/r/gvenzl/oracle-xe):
```sh
docker pull gvenzl/oracle-xe
```
- Create a `docker-compose.yaml` file with the following content:
```yaml
version: "3.9"
services:
  db-oracle:
    image: gvenzl/oracle-xe:latest
    environment:
      - ORACLE_PASSWORD=%123456$
    ports:
      - "1521:1521"
```
- Setup the container:
```sh
docker-compose up -d db-oracle
docker-compose logs -f db-oracle
```
> #### Optional:
> In case you need to reset the admin password:
> ```sh
> docker exec oracle-db resetPassword "%123456$"
> ```

- To connect to the oracle container and log into the db (after the `sqlplus` command, user and password will be requested):
```sh
docker-compose exec oracle-db bash
sqlplus system@XEPDB1
```

## Configuration

- set the following variable to configure the database after installation:
https://logic.edchen.org/how-to-resolve-ins-08101-unexpected-error/

> #### Important:
> If you forgot the password introduced on the configuration process, you can reconfigure oracle db, performing the following:
> ```sh
> /etc/init.d/oracle-xe-21c delete
> /etc/init.d/oracle-xe-21c configure
> ```

- after configuration process, the following message will be displayed:
```sh
Connect to Oracle Database using one of the connect strings:
     Pluggable database: oracle-db/XEPDB1
     Multitenant container database: oracle-db
Use https://localhost:5500/em to access Oracle Enterprise Manager for Oracle Database XE
```

## Usage

> #### Important:
> - To log into oracle database by terminal:
> ```sh
> sqlplus <user>@xepdb1
> ```
> - To download testing data:
> ```sh
> curl -LJO https://raw.githubusercontent.com/gvenzl/sample-data/master/countries-cities-currencies/install.sql
> sqlplus <user>@xepdb1
> @install.sql
> ```

Once you have logged into oracle db by terminal using `sqlplus`, you will able to performs some of the following operations:

- Describe table structure:
```sql
describe user_tables;
describe all_all_tables;
describe dba_tables;
describe all_views;
describe user_tab_columns;
describe dba_users;
```
- To create a new oracle account:
```sql
select username, default_tablespace from dba_users where username = '<username>';
create user <username> identified by "%123456$" quota unlimited on users;
alter user <username> identified by "%123456$";
alter user <username> quote unlimited on users;
grant create session to <username>;
grant create session to <username> identified by "<passwd>";
grant create table, create routine, create view, create sequence, drop table, alter any table to <username>;
grant select, update, delete, insert on <db-owner>.<db-object> to <username>;
```


## References

- start oracle service:
https://docs.oracle.com/en/database/oracle/oracle-database/21/xeinl/starting-and-stopping-oracle-database.html#GUID-88F1B209-A4DA-4EC7-B544-AC6351793C8A

- connecting to a oracledb
https://docs.oracle.com/en/database/oracle/oracle-database/21/xeinl/connecting-oracle-database-free.html#GUID-C58490E7-3918-4536-AC10-D2819BD29F10
