# LPI Notes

Some important information to remember about LPI.

Glossary:
- [Commands](#commands)
	- [Shell](#shell)
	- [User Administration](#user-administration)
- [Files](#files)
	- [Configuration Files](#configuration-files)
	- [Find Files](#find-files)
	- [Server Status](#server-status)
- [Devices](#devices)
	- [Device Discovery](#device-discovery)
	- [Device Files](#device-files)
	- [Kernel Modules](#kernel-modules)
	- [Kernel Configurations](#kernel-configurations)

## Commands

### Shell

- **set** : set env parameters.
- **export** : set env parameters
- **env** : set env parameters
- **type** : identify if a command is an internal / external one.
- **file** : identify the type of file, based on details of the file `/usr/share/magic`.
- **sort** : to sort a specific content, variables that be used here `LC_ALL, LC_COLLATE, LANG`, for example:
```sh
LC_ALL=en_us.UTF-8 sort
# to list all availables locales
localectl list-locales
```
- **date** : to get / set system date, variables that can be used here `TZ=America/Lima date`, if you want to know other timezones execute the command:
```sh
timedatectl list-timezones
```
- **IFS** : important variable that is used as a delimiter through `while, for` clauses, for example:
```sh
echo "usertest|passwdtest" | while IFS='|' read user pass ; do echo $user ; done
```
- **kill** : kill processes by sending signals to a process ID, if you want to know available signals `kill -l`.
- **killall** : kill processes based on command name.
- **jobs** : will list processes running in the background.
- **fg** : allows to bring back to the active shell a process of the background.
- **ps** : list all processes running on the system.
- **pstree** : list processes running on the system in a tree mode.
- **pgrep** : find processes based on the command name or full command specification and return its processes IDs.
- **pkill** : kill processes based on the command name or full command specification, an advantage to the `kill` command is that this can be more specific at the moment to look for a process.

- **nice** : allows to set a specific value to a process ID from `-20` to `+19` (default value is `+10`), and based on that the kernel quantifies the degree of favouritism towards the process and its child processes. The lower the value, the more priority it will receive.
- **renice** : allows to update the nice value of a process, take into consideration that just `root` can increase or decrease the value, as a single user you will be able just to increment the value.

- **top** : shows a table that is frequently updated having details of the system such: processes, memory use, cpu use, etc.
- **nohup** : allows to execute a command that will ignore the `SIGHUP` signal and thus, survive the demise of its parent process. The outcome of the command will be placed into a file called **`nohup.out`**.

- **ulimit** : modify shell resource limits.

### User Administration

- **lchfn** : change the `GECOS` field of the `/etc/passwd` file.
- **lchsh** : change the shell field of the `/etc/passwd` file, based on the values of the file `/etc/shells`.
- **gpasswd** : `add` / `remove` users from specific groups.
- **chage** : it modify information of the `/etc/shadow`.
- **getent**:  get information about an account or group querying information to all sources (text files, ldap, etc), file having details of the sources `/etc/nsswitch.conf¡`.
- **vipw** : modify the `/etc/passwd` directly, with `-s` modifies the `/etc/shadow`.
- **vigr** : modify the `/etc/group` directly, with `-s` modifies the `/etc/gshadow`.

## Files

### Configuration Files

- **/etc/issue** : greetings before the user type the passwd.
- **/etc/motd** : message of the day, once the user has logged.

### Find Files

- **/usr/share/dict/words** : file containing a lot of words for testing purposes, in case file does not exist on your system, it can be installed with the package `words`.
- **/usr/bin/locate** : command to find system files from a database that is built with the command `updatedb`, if file is not available on your system, it can be installed with the package `mlocate`.

### Server Status

- **/var/log/wtmp** : file having information about last logins, if read permissions are removed, normal users won't be able to execute the command `last`.
- **/proc/cpuinfo** : information about cpu use.
- **/proc/meminfo** : information about memory use.
- **/proc/loadavg** : information about load avg.
- **/proc/mounts** : have detailed information about the current mounts of the system, this file is a symbolic link of the file `/proc/self/mounts` as well as `/etc/mtab`.

## Devices

### Device Discovery

> ### Important!
> If some of the programs are not found in the system, try to query if the following packages are installed:
> ```sh
> rpm -qa usbutils
> rpm -qa pciutils
> rpm -qa lsscsi
> rpm -qa lsblk
> ```
> If they are not installed, proceed to execute the following:
> ```sh
> yum install -y pciutils usbutils lssci util-linux
> ```

- **lspci** : list all PCI devices of the system, accepts the following flags `-v`, `-t` and `-n`.
- **lsusb** : list all peripherals of usb connection type.
- **udev** : dynamic devices management in charge of the `/dev` directory.
- **lsscsi** : list scsi devices (small computer system interface).
- **lsblk** : list storage devices and volumes.
- **udisksctl** : udisk command line that shows information about available disks. The command works along with `udisks` daemon, examples:
```sh
udisksctl status
udisksctl info -b /dev/<diskname>
```

- **dd** : allows you to create images, create partition backups and format pendrives.
	- To format a pendriver (`bs` byte sequence, `count` times that byte sequence will be executed):
	```sh
	dd if=/dev/zero of=disk-small.img bs=1M count=1024
	```
- **fdisk** : to create / remove / list partitions of a device (hard disk), some helpful operations:
	- Get disk information:
	```sh
	fdisk -l disk-small.img
	```
	- Get sector / block size (similar to `blockdev --getbsz /dev/vda`):
	```sh
	fdisk -l disk-small.img |egrep -i "Sector Size"
	```
- **gdisk** : to create / remove / list partitions of a device, oriented on GPT partition tables.
- **parted** : like fdisk and gdisk command, but more interactive, for example:
	- To create a partition table and a single partition in one line:
	```sh
	parted disk-small.img unit mb mklabel msdos mkpart primary ext4 12MB 512MB p free
	```
	- To remove a partition:
	```sh
	parted disk-small.img rm <index-partition>
	```
	- To list disk free space:
	```sh
	parted disk-small.img p free
	```
	- To display helpful information of a specific command:
	```sh
	parted disk-small.img help set
	```
	- To set a flag to a specific partition:
	```sh
	parted disk-small.img set <index-partition> lvm on
	```
- **losetup** : create loop devices bound to an image.
	- To create a loop device:
	```sh
	losetup -f <image-file>
	```
	- To check all loop devices:
	```sh
	losetup -l
	```
	- To detach all loop devices:
	```sh
	losetup -d <image-file>
	losetup -D
	```
- **kpartx** : create device files for the partitions of a loop device.
	- To check all loop devices:
	```sh
	kpartx -av <loop-device>
	```
	- To detach all partitions from loop device:
	```sh
	kpartx -dv <loop-device>
	```

### Device Files

Files that represents the connected devices are located under the directory `/dev/`:
```sh
file /dev/urandom
find /dev/ -type c
```

- **cups** : printer service, usually user applications send information to `cups` for printing, then cups access the information of selected printer (maker, model, etc) and based on that information pass them to the `kernel` to choose the right device.

### Kernel modules

Modules are located under the directory `/lib/modules/`.

- **modpro** : load / unload a specific module from the kernel, to unload a module `modprobe -r <module-name>`. Configuration files are `/etc/modprobe.conf` or `/etc/modprobe.d/`.
- **lsmod** : list current loaded modules.

### Kernel Configurations

There is a directory which was created as a result of the Kernel developers community demands that is **`sysfs`** that is actually mounted on the path **`/sys`**, you can find out about it performing the following command:
```sh
mount -l |grep -i sysfs
```

It works as a interface to the kernel, specifically, it provides information and configuration settings that the kernel provides.

Some additional details that can be found in that directory are the devices connected to your system and based on its connector type (`pci`,`usb`,`scsci`, etc) they are accessible, such as:
```sh
ls -lF /sys/bus/{pci,usb,scsi}
```

Some other important directories:
```sh
ls -lFd  /sys/{bus,devices,block,class,net}
```
- **bus** : devices parent directory.
- **devices** : devices directory which are bound to `bus` directory by symbolic links.
- **block** : directory having all block devices.
- **class** : directory having all character devices.
- **net**: provides network configuration settings.

