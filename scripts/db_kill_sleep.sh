
# credentials 
DATABASE="database"
SERVER="hostname"
USERNAME="username"
PASSWD="password"
PORT="3306"

NOW="/bin/date +\"%Y-%m-%d %H:%M:%S\""
MYSQL_INIT_CMD="mysql --user=${USERNAME} --password=${PASSWD} -h ${SERVER} ${DATABASE} -N "
# quantity is expressed in seconds
LIMIT_VALUE=50
MYSQL_SENTENCE="select concat(\"kill \",id,\";\") as cmd from information_schema.PROCESSLIST as p where p.command regexp 'sleep' and p.time >= ${LIMIT_VALUE};"
TEMP_FILE="/tmp/jobs_tmp"

echo "$(eval ${NOW}) - Processing killing sleep process on mysql"

${MYSQL_INIT_CMD} -e "${MYSQL_SENTENCE}" > ${TEMP_FILE}
cat ${TEMP_FILE} | while read line
do
	${MYSQL_INIT_CMD} -e "${line}"
done

if [ $? -eq 0 ]; then
   echo "$(eval ${NOW}) - Process has finished killing $(cat ${TEMP_FILE}|wc -l) ."
fi

