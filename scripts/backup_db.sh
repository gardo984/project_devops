
DATABASE="database"
SERVER="hostname"
USERNAME="username"
PASSWD="password"
PORT="3306"
TIME=$(/bin/date +"%Y%m%d%H%M%S")
NOW="/bin/date +\"%Y-%m-%d %H:%M:%S\""
FILENAME="${DATABASE}_${TIME}.sql"
PATH="/var/mysql_backups/"
PATH_NAS="/mnt/backup_db/"

cd ${PATH}
echo "$(eval ${NOW}) - Processing database backup ${DATABASE}..."
/usr/bin/mysqldump --user=${USERNAME} --password=${PASSWD} -h ${SERVER} --databases ${DATABASE} --single-transaction --opt --routines --triggers > ${FILENAME}
if [ $? -eq 0 ] ; then
	echo "$(eval ${NOW}) - File ${PATH}${FILENAME} was generated successfully"
	TARFILE="${PATH}${FILENAME}.gz"
	SOURCE="${PATH}${FILENAME}"
	/usr/bin/sleep 3
	/usr/bin/gzip -f ${SOURCE}
	if [ $? -eq 0 ] ; then
		echo "$(eval ${NOW}) - File ${TARFILE} was generated successfully"
		/usr/bin/mv -v ${TARFILE} ${PATH_NAS}
		echo "$(eval ${NOW}) - File ${TARFILE} is been moved to ${PATH_NAS}"
		echo "$(eval ${NOW}) - Removing unneccesary files..."
		/usr/bin/rm -rf $(/usr/bin/ls| /usr/bin/grep -vi ${FILENAME})
	else
		echo "$(eval ${NOW}) - Error zipping file ${TARFILE}."
	fi
else
	echo "$(eval ${NOW}) - Error processing backup."
fi

echo "$(eval ${NOW}) - Process has finished."

