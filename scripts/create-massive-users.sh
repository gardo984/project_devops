#!/bin/bash

createUserList(){
	tmp_file=/tmp/users
	for n in $(seq 1 100); do echo "operator$n|$(openssl rand -hex 10)" ; done > $tmp_file
	test -f $tmp_file && echo "User list successfully created" || echo "Error, user list could not be created"
}
createUsers(){
	tmp_file=/tmp/users
	cat $tmp_file| while IFS="|" read user pass; do useradd --password $(openssl passwd -5 $pass) $user && chage -d 0 $user ; done
	echo "User were successfully created"
}
removeUsers(){
	cat /tmp/users| while IFS="|" read user pass; do userdel -rf $user ; done
	echo "User were successfully removed"	
}

echo "What operation do you want to execute?"
printf "1) Create User Random List\n2) Create users\n3) Remove users\n"
echo -n "Enter option: "
read operation

case $operation in
	"1")
		createUserList
		;;
	"2")
		createUsers
		;;
	"3")
		removeUsers
		;;
esac